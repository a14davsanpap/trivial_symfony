<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/partida")
     */
    public function indexAction(Request $request)
    {
        $nom_jugador = $this->getDoctrine()
        ->getRepository('AppBundle:jugador')
        ->findAll();
        return $this->render('jugador.html.twig', array('viewjugador' => $nom_jugador));
    }
    
       /**
     * @Route("/opcio", name="opcio")
     */
    public function opcioAction(Request $request)
    {
        return $this->render('opcions.html.twig');
    }

}