<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DausController extends Controller
{
    /**
     * @Route("/partida", name="partida2")
     */
    public function numberAction()
    {
        $number = mt_rand(1, 6);

        return $this->render('jugador.html.twig', array(
            'number' => $number,
        ));
    }
  
}