<?php

// src/AppBundle/Entity/jugador.php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="jugador")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\jugadorRepository")
 */

class jugador
{
     /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idjugador;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $nickjugador;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $password;

    /**
     * Get idjugador
     *
     * @return integer
     */
    public function getIdjugador()
    {
        return $this->idjugador;
    }

    /**
     * Set nickjugador
     *
     * @param string $nickjugador
     *
     * @return jugador
     */
    public function setNickjugador($nickjugador)
    {
        $this->nickjugador = $nickjugador;

        return $this;
    }

    /**
     * Get nickjugador
     *
     * @return string
     */
    public function getNickjugador()
    {
        return $this->nickjugador;
    }

    /**
     * Set password
     *
     * @param integer $password
     *
     * @return jugador
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return integer
     */
    public function getPassword()
    {
        return $this->password;
    }
}
