<?php

// src/AppBundle/Entity/quesera.php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="quesera")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\queseraRepository")
 */

class quesera
{
     /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idjugador;

     /**
     * @ORM\Column(type="integer")
     */
    protected $idpartida;

    /**
     * @ORM\Column(type="integer", scale=2)
     */
    protected $posicio;
    
        /**
     * @ORM\Column(type="integer", scale=2)
     */
    protected $quesito;

    /**
     * Get idjugador
     *
     * @return integer
     */
    public function getIdjugador()
    {
        return $this->idjugador;
    }

    /**
     * Set idpartida
     *
     * @param integer $idpartida
     *
     * @return quesera
     */
    public function setIdpartida($idpartida)
    {
        $this->idpartida = $idpartida;

        return $this;
    }

    /**
     * Get idpartida
     *
     * @return integer
     */
    public function getIdpartida()
    {
        return $this->idpartida;
    }

    /**
     * Set posicio
     *
     * @param integer $posicio
     *
     * @return quesera
     */
    public function setPosicio($posicio)
    {
        $this->posicio = $posicio;

        return $this;
    }

    /**
     * Get posicio
     *
     * @return integer
     */
    public function getPosicio()
    {
        return $this->posicio;
    }

    /**
     * Set quesito
     *
     * @param integer $quesito
     *
     * @return quesera
     */
    public function setQuesito($quesito)
    {
        $this->quesito = $quesito;

        return $this;
    }

    /**
     * Get quesito
     *
     * @return integer
     */
    public function getQuesito()
    {
        return $this->quesito;
    }
}
