<?php

// src/AppBundle/Entity/partida.php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="partida")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\partidaRepository")
 */

class partida
{
     /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idpartida;

    /**
     * @ORM\Column(type="integer", scale=2)
     */
    protected $estado;

    /**
     * Get idpartida
     *
     * @return integer
     */
    public function getIdpartida()
    {
        return $this->idpartida;
    }

    /**
     * Set estado
     *
     * @param integer $estado
     *
     * @return partida
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return integer
     */
    public function getEstado()
    {
        return $this->estado;
    }
}
