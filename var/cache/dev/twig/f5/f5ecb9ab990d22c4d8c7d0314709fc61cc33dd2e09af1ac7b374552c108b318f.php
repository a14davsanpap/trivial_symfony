<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_11fbc2198b9cff59e44d0de36bc9cd6575455705f2ca6dcc7421f23ea21bc1d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c83802d8157c8deb7d7b3c8e11e2e285c0556490bc0a8046aa78f1c3c54e0c39 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c83802d8157c8deb7d7b3c8e11e2e285c0556490bc0a8046aa78f1c3c54e0c39->enter($__internal_c83802d8157c8deb7d7b3c8e11e2e285c0556490bc0a8046aa78f1c3c54e0c39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        $__internal_787aa329e5ccde45d4cc55842c85b4a45285dc5d725f52a1066878363867aeaf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_787aa329e5ccde45d4cc55842c85b4a45285dc5d725f52a1066878363867aeaf->enter($__internal_787aa329e5ccde45d4cc55842c85b4a45285dc5d725f52a1066878363867aeaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => ($context["exception"] ?? $this->getContext($context, "exception")))));
        
        $__internal_c83802d8157c8deb7d7b3c8e11e2e285c0556490bc0a8046aa78f1c3c54e0c39->leave($__internal_c83802d8157c8deb7d7b3c8e11e2e285c0556490bc0a8046aa78f1c3c54e0c39_prof);

        
        $__internal_787aa329e5ccde45d4cc55842c85b4a45285dc5d725f52a1066878363867aeaf->leave($__internal_787aa329e5ccde45d4cc55842c85b4a45285dc5d725f52a1066878363867aeaf_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}
", "TwigBundle:Exception:exception.rdf.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}
