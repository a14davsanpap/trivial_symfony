<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_d9ffae65641b2646830901a5f86c06bf780d0129aa15524b0693f8abf9d9bfc6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3b288eea3e507329c828c506169f1df47a96ab06e6714e70567afdc3a9716760 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3b288eea3e507329c828c506169f1df47a96ab06e6714e70567afdc3a9716760->enter($__internal_3b288eea3e507329c828c506169f1df47a96ab06e6714e70567afdc3a9716760_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_9ff7701bef1630fdee5ee720741aabf634e808e364d25abf345cadbf30582abd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ff7701bef1630fdee5ee720741aabf634e808e364d25abf345cadbf30582abd->enter($__internal_9ff7701bef1630fdee5ee720741aabf634e808e364d25abf345cadbf30582abd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3b288eea3e507329c828c506169f1df47a96ab06e6714e70567afdc3a9716760->leave($__internal_3b288eea3e507329c828c506169f1df47a96ab06e6714e70567afdc3a9716760_prof);

        
        $__internal_9ff7701bef1630fdee5ee720741aabf634e808e364d25abf345cadbf30582abd->leave($__internal_9ff7701bef1630fdee5ee720741aabf634e808e364d25abf345cadbf30582abd_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_b519bf5337606049f3bc024355edf26a50fceb96400ca235432d8aaa1d43a4b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b519bf5337606049f3bc024355edf26a50fceb96400ca235432d8aaa1d43a4b2->enter($__internal_b519bf5337606049f3bc024355edf26a50fceb96400ca235432d8aaa1d43a4b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_9b30d84a9ea946effa82ed777238eb3938884d1b088757d38a259c4b016bfd9f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9b30d84a9ea946effa82ed777238eb3938884d1b088757d38a259c4b016bfd9f->enter($__internal_9b30d84a9ea946effa82ed777238eb3938884d1b088757d38a259c4b016bfd9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_9b30d84a9ea946effa82ed777238eb3938884d1b088757d38a259c4b016bfd9f->leave($__internal_9b30d84a9ea946effa82ed777238eb3938884d1b088757d38a259c4b016bfd9f_prof);

        
        $__internal_b519bf5337606049f3bc024355edf26a50fceb96400ca235432d8aaa1d43a4b2->leave($__internal_b519bf5337606049f3bc024355edf26a50fceb96400ca235432d8aaa1d43a4b2_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_f36c875f8cdf42002f886c747515d97d678616b4b294f0ec48027bd10dd6ce60 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f36c875f8cdf42002f886c747515d97d678616b4b294f0ec48027bd10dd6ce60->enter($__internal_f36c875f8cdf42002f886c747515d97d678616b4b294f0ec48027bd10dd6ce60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_16687b05f70629311c3788c232585c7ca094d558cec91566c58164038ed76af0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_16687b05f70629311c3788c232585c7ca094d558cec91566c58164038ed76af0->enter($__internal_16687b05f70629311c3788c232585c7ca094d558cec91566c58164038ed76af0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_16687b05f70629311c3788c232585c7ca094d558cec91566c58164038ed76af0->leave($__internal_16687b05f70629311c3788c232585c7ca094d558cec91566c58164038ed76af0_prof);

        
        $__internal_f36c875f8cdf42002f886c747515d97d678616b4b294f0ec48027bd10dd6ce60->leave($__internal_f36c875f8cdf42002f886c747515d97d678616b4b294f0ec48027bd10dd6ce60_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
