<?php

/* FOSUserBundle:Resetting:check_email.html.twig */
class __TwigTemplate_d6eb2c0cab69b0eadcfffdc44486046040fe0cc2e95383738185480afdad515d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bab0e779cd5662d565dc0f356d67c4fda21116de5d9dac4c3320b6e62eab43a3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bab0e779cd5662d565dc0f356d67c4fda21116de5d9dac4c3320b6e62eab43a3->enter($__internal_bab0e779cd5662d565dc0f356d67c4fda21116de5d9dac4c3320b6e62eab43a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $__internal_6fa9c2ee6aa85cc7e9beffd44e078ba02c44e941776748dbe06ad69ee59b21c1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6fa9c2ee6aa85cc7e9beffd44e078ba02c44e941776748dbe06ad69ee59b21c1->enter($__internal_6fa9c2ee6aa85cc7e9beffd44e078ba02c44e941776748dbe06ad69ee59b21c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bab0e779cd5662d565dc0f356d67c4fda21116de5d9dac4c3320b6e62eab43a3->leave($__internal_bab0e779cd5662d565dc0f356d67c4fda21116de5d9dac4c3320b6e62eab43a3_prof);

        
        $__internal_6fa9c2ee6aa85cc7e9beffd44e078ba02c44e941776748dbe06ad69ee59b21c1->leave($__internal_6fa9c2ee6aa85cc7e9beffd44e078ba02c44e941776748dbe06ad69ee59b21c1_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_140e084fd26b79b59cbf43cdf5dbf55ba55cf760b43e78b96f1e1545bddf1cae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_140e084fd26b79b59cbf43cdf5dbf55ba55cf760b43e78b96f1e1545bddf1cae->enter($__internal_140e084fd26b79b59cbf43cdf5dbf55ba55cf760b43e78b96f1e1545bddf1cae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_68b838d7e7f1eae4558cec0f70c9297431d957c888d9434fa6d560f78991892e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_68b838d7e7f1eae4558cec0f70c9297431d957c888d9434fa6d560f78991892e->enter($__internal_68b838d7e7f1eae4558cec0f70c9297431d957c888d9434fa6d560f78991892e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => ($context["tokenLifetime"] ?? $this->getContext($context, "tokenLifetime"))), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_68b838d7e7f1eae4558cec0f70c9297431d957c888d9434fa6d560f78991892e->leave($__internal_68b838d7e7f1eae4558cec0f70c9297431d957c888d9434fa6d560f78991892e_prof);

        
        $__internal_140e084fd26b79b59cbf43cdf5dbf55ba55cf760b43e78b96f1e1545bddf1cae->leave($__internal_140e084fd26b79b59cbf43cdf5dbf55ba55cf760b43e78b96f1e1545bddf1cae_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
<p>
{{ 'resetting.check_email'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}
</p>
{% endblock %}
", "FOSUserBundle:Resetting:check_email.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/check_email.html.twig");
    }
}
