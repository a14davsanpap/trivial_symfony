<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_d09f5d001bdfae6a908da2fca527dcc57f94534cbb2a2fd4d8c1f862b601f725 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_14070c9abc302bddc06519667f4d7baf803955ee8381efa4f3ef3dbe4ac2e11a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_14070c9abc302bddc06519667f4d7baf803955ee8381efa4f3ef3dbe4ac2e11a->enter($__internal_14070c9abc302bddc06519667f4d7baf803955ee8381efa4f3ef3dbe4ac2e11a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $__internal_0b183b9f22d6272b23b25eae63c055062c2a95e29b918d599fee5a8a8a5e1112 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b183b9f22d6272b23b25eae63c055062c2a95e29b918d599fee5a8a8a5e1112->enter($__internal_0b183b9f22d6272b23b25eae63c055062c2a95e29b918d599fee5a8a8a5e1112_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("purge" => array("status" => "success", "title" => "The profiler database was purged successfully", "message" => "Now you need to browse some pages with the Symfony Profiler enabled to collect data."), "no_token" => array("status" => "error", "title" => (((((        // line 11
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 12
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))), "upload_error" => array("status" => "error", "title" => "A problem occurred when uploading the data", "message" => "No file given or the file was not uploaded successfully."), "already_exists" => array("status" => "error", "title" => "A problem occurred when uploading the data", "message" => "The token already exists in the database."));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_14070c9abc302bddc06519667f4d7baf803955ee8381efa4f3ef3dbe4ac2e11a->leave($__internal_14070c9abc302bddc06519667f4d7baf803955ee8381efa4f3ef3dbe4ac2e11a_prof);

        
        $__internal_0b183b9f22d6272b23b25eae63c055062c2a95e29b918d599fee5a8a8a5e1112->leave($__internal_0b183b9f22d6272b23b25eae63c055062c2a95e29b918d599fee5a8a8a5e1112_prof);

    }

    // line 26
    public function block_summary($context, array $blocks = array())
    {
        $__internal_6845e40f0924fe59fd5962d341a0e529860d204d698772513a0ee3f740c33d82 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6845e40f0924fe59fd5962d341a0e529860d204d698772513a0ee3f740c33d82->enter($__internal_6845e40f0924fe59fd5962d341a0e529860d204d698772513a0ee3f740c33d82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_d54bae88f09f83cca05be51321d086261397a8c5fefe84e09c53e7980b926f36 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d54bae88f09f83cca05be51321d086261397a8c5fefe84e09c53e7980b926f36->enter($__internal_d54bae88f09f83cca05be51321d086261397a8c5fefe84e09c53e7980b926f36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 27
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 29
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_d54bae88f09f83cca05be51321d086261397a8c5fefe84e09c53e7980b926f36->leave($__internal_d54bae88f09f83cca05be51321d086261397a8c5fefe84e09c53e7980b926f36_prof);

        
        $__internal_6845e40f0924fe59fd5962d341a0e529860d204d698772513a0ee3f740c33d82->leave($__internal_6845e40f0924fe59fd5962d341a0e529860d204d698772513a0ee3f740c33d82_prof);

    }

    // line 34
    public function block_panel($context, array $blocks = array())
    {
        $__internal_82b33e349bf32c492546e799454e5be2866168058c84a85cba5f821662b49c80 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_82b33e349bf32c492546e799454e5be2866168058c84a85cba5f821662b49c80->enter($__internal_82b33e349bf32c492546e799454e5be2866168058c84a85cba5f821662b49c80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_0c2d319454d3b3ae11d51e3eac3c1d5f09d6939a151295d6c1063ec7c7fbde94 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0c2d319454d3b3ae11d51e3eac3c1d5f09d6939a151295d6c1063ec7c7fbde94->enter($__internal_0c2d319454d3b3ae11d51e3eac3c1d5f09d6939a151295d6c1063ec7c7fbde94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 35
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_0c2d319454d3b3ae11d51e3eac3c1d5f09d6939a151295d6c1063ec7c7fbde94->leave($__internal_0c2d319454d3b3ae11d51e3eac3c1d5f09d6939a151295d6c1063ec7c7fbde94_prof);

        
        $__internal_82b33e349bf32c492546e799454e5be2866168058c84a85cba5f821662b49c80->leave($__internal_82b33e349bf32c492546e799454e5be2866168058c84a85cba5f821662b49c80_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 36,  84 => 35,  75 => 34,  61 => 29,  55 => 27,  46 => 26,  36 => 1,  34 => 12,  33 => 11,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'purge' : {
        status:  'success',
        title:   'The profiler database was purged successfully',
        message: 'Now you need to browse some pages with the Symfony Profiler enabled to collect data.'
    },
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    },
    'upload_error' : {
        status:  'error',
        title:   'A problem occurred when uploading the data',
        message: 'No file given or the file was not uploaded successfully.'
    },
    'already_exists' : {
        status:  'error',
        title:   'A problem occurred when uploading the data',
        message: 'The token already exists in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}
