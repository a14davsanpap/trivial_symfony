<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_17c490066699502e69bb6beceb589e61795079c9b41ab7aee2b779ce4a04f654 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0916ce5a9165f20d0f677fe1ca551369960af919a4e778abb18d1bc45970d1eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0916ce5a9165f20d0f677fe1ca551369960af919a4e778abb18d1bc45970d1eb->enter($__internal_0916ce5a9165f20d0f677fe1ca551369960af919a4e778abb18d1bc45970d1eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $__internal_6d78a6448ab5f16059c600ab43800e1e151bb0fabe37162de76d369de46bb88c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d78a6448ab5f16059c600ab43800e1e151bb0fabe37162de76d369de46bb88c->enter($__internal_6d78a6448ab5f16059c600ab43800e1e151bb0fabe37162de76d369de46bb88c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0916ce5a9165f20d0f677fe1ca551369960af919a4e778abb18d1bc45970d1eb->leave($__internal_0916ce5a9165f20d0f677fe1ca551369960af919a4e778abb18d1bc45970d1eb_prof);

        
        $__internal_6d78a6448ab5f16059c600ab43800e1e151bb0fabe37162de76d369de46bb88c->leave($__internal_6d78a6448ab5f16059c600ab43800e1e151bb0fabe37162de76d369de46bb88c_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ae1fd7a3989f03c57ab55a207241a689fc6eb1f8ae540eeddeb92ccec534bb06 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae1fd7a3989f03c57ab55a207241a689fc6eb1f8ae540eeddeb92ccec534bb06->enter($__internal_ae1fd7a3989f03c57ab55a207241a689fc6eb1f8ae540eeddeb92ccec534bb06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_4a24384f216e3f3acb700446fbcc6d8a6d76c1aab744ffb5231b552e4dfae038 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a24384f216e3f3acb700446fbcc6d8a6d76c1aab744ffb5231b552e4dfae038->enter($__internal_4a24384f216e3f3acb700446fbcc6d8a6d76c1aab744ffb5231b552e4dfae038_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_4a24384f216e3f3acb700446fbcc6d8a6d76c1aab744ffb5231b552e4dfae038->leave($__internal_4a24384f216e3f3acb700446fbcc6d8a6d76c1aab744ffb5231b552e4dfae038_prof);

        
        $__internal_ae1fd7a3989f03c57ab55a207241a689fc6eb1f8ae540eeddeb92ccec534bb06->leave($__internal_ae1fd7a3989f03c57ab55a207241a689fc6eb1f8ae540eeddeb92ccec534bb06_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:edit.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/edit.html.twig");
    }
}
