<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_bbc52713541d035994e8865cb299655a6527c2d9a87235596b02f0d9ec8ee303 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2d03bfef8674d197a04c7a5f2f98b14d6e591293cd11b376ecaaead0c2bea8ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2d03bfef8674d197a04c7a5f2f98b14d6e591293cd11b376ecaaead0c2bea8ec->enter($__internal_2d03bfef8674d197a04c7a5f2f98b14d6e591293cd11b376ecaaead0c2bea8ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        $__internal_7b90a4e1172fd83f8258cffff1b31d833a7d154fed847e08e55d500555ca09d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7b90a4e1172fd83f8258cffff1b31d833a7d154fed847e08e55d500555ca09d3->enter($__internal_7b90a4e1172fd83f8258cffff1b31d833a7d154fed847e08e55d500555ca09d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_2d03bfef8674d197a04c7a5f2f98b14d6e591293cd11b376ecaaead0c2bea8ec->leave($__internal_2d03bfef8674d197a04c7a5f2f98b14d6e591293cd11b376ecaaead0c2bea8ec_prof);

        
        $__internal_7b90a4e1172fd83f8258cffff1b31d833a7d154fed847e08e55d500555ca09d3->leave($__internal_7b90a4e1172fd83f8258cffff1b31d833a7d154fed847e08e55d500555ca09d3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rows.html.php");
    }
}
