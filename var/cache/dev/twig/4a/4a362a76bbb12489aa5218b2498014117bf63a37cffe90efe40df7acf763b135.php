<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_464c6167f8ca2490d2a0afd51fb369b2b491bfb9f9e72d194875d9901ba2d706 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_586cb712d6f091718189e828560d6da5e478b37b546d58d111cf05c5a860f21d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_586cb712d6f091718189e828560d6da5e478b37b546d58d111cf05c5a860f21d->enter($__internal_586cb712d6f091718189e828560d6da5e478b37b546d58d111cf05c5a860f21d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_1872cf86ddcb486450d43bed996c95a018d3cebc2f7f79e9afc250777123afd1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1872cf86ddcb486450d43bed996c95a018d3cebc2f7f79e9afc250777123afd1->enter($__internal_1872cf86ddcb486450d43bed996c95a018d3cebc2f7f79e9afc250777123afd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_586cb712d6f091718189e828560d6da5e478b37b546d58d111cf05c5a860f21d->leave($__internal_586cb712d6f091718189e828560d6da5e478b37b546d58d111cf05c5a860f21d_prof);

        
        $__internal_1872cf86ddcb486450d43bed996c95a018d3cebc2f7f79e9afc250777123afd1->leave($__internal_1872cf86ddcb486450d43bed996c95a018d3cebc2f7f79e9afc250777123afd1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/number_widget.html.php");
    }
}
