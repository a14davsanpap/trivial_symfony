<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_72111f2bb34a4692c3c0a7e55cc41e467ad2b8c3530294bfd71f5611508f0b06 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7d0b948942c2f215e038a4cc3253019c143d339d88777693547ddebd1aa32785 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7d0b948942c2f215e038a4cc3253019c143d339d88777693547ddebd1aa32785->enter($__internal_7d0b948942c2f215e038a4cc3253019c143d339d88777693547ddebd1aa32785_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        $__internal_5d173fca3cc89363da8c7ff493c0e09f879d691d453561d034e8fd525b77707f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5d173fca3cc89363da8c7ff493c0e09f879d691d453561d034e8fd525b77707f->enter($__internal_5d173fca3cc89363da8c7ff493c0e09f879d691d453561d034e8fd525b77707f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_7d0b948942c2f215e038a4cc3253019c143d339d88777693547ddebd1aa32785->leave($__internal_7d0b948942c2f215e038a4cc3253019c143d339d88777693547ddebd1aa32785_prof);

        
        $__internal_5d173fca3cc89363da8c7ff493c0e09f879d691d453561d034e8fd525b77707f->leave($__internal_5d173fca3cc89363da8c7ff493c0e09f879d691d453561d034e8fd525b77707f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
", "TwigBundle:Exception:error.json.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.json.twig");
    }
}
