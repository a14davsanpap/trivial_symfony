<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_fea3a793720e7e3ca582497db3bcbbd044a3fdefbfd6759e0bc06bfffe26ad1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a2550aff81ab7678ea2bc5095c19d9fe2eef3f0217778481e9b89a50b11896d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a2550aff81ab7678ea2bc5095c19d9fe2eef3f0217778481e9b89a50b11896d4->enter($__internal_a2550aff81ab7678ea2bc5095c19d9fe2eef3f0217778481e9b89a50b11896d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_8ffc5da88858c9c6102376121faf65981faee48beb6609966ad9f6cd9f5a96d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8ffc5da88858c9c6102376121faf65981faee48beb6609966ad9f6cd9f5a96d4->enter($__internal_8ffc5da88858c9c6102376121faf65981faee48beb6609966ad9f6cd9f5a96d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_a2550aff81ab7678ea2bc5095c19d9fe2eef3f0217778481e9b89a50b11896d4->leave($__internal_a2550aff81ab7678ea2bc5095c19d9fe2eef3f0217778481e9b89a50b11896d4_prof);

        
        $__internal_8ffc5da88858c9c6102376121faf65981faee48beb6609966ad9f6cd9f5a96d4->leave($__internal_8ffc5da88858c9c6102376121faf65981faee48beb6609966ad9f6cd9f5a96d4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}
