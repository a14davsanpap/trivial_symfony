<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_93e6d97fe1779f5e1ad3aeb45ba946d842a23dff8c07c99deb961e708599fd1c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_efc6e6c6e849aa36ebe4768842081589ff7e0131072017d04e3e2f808a5e12df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_efc6e6c6e849aa36ebe4768842081589ff7e0131072017d04e3e2f808a5e12df->enter($__internal_efc6e6c6e849aa36ebe4768842081589ff7e0131072017d04e3e2f808a5e12df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_5c3748149f324d7cdf37f23e13be380cc8641249a6a4d9157d92566099c8e403 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5c3748149f324d7cdf37f23e13be380cc8641249a6a4d9157d92566099c8e403->enter($__internal_5c3748149f324d7cdf37f23e13be380cc8641249a6a4d9157d92566099c8e403_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_efc6e6c6e849aa36ebe4768842081589ff7e0131072017d04e3e2f808a5e12df->leave($__internal_efc6e6c6e849aa36ebe4768842081589ff7e0131072017d04e3e2f808a5e12df_prof);

        
        $__internal_5c3748149f324d7cdf37f23e13be380cc8641249a6a4d9157d92566099c8e403->leave($__internal_5c3748149f324d7cdf37f23e13be380cc8641249a6a4d9157d92566099c8e403_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/email_widget.html.php");
    }
}
