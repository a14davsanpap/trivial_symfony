<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_e7081212769a60c26de484ba6ccababa88b354f06e09716a5d656b17f0684a1a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_32fe0b6fc752eca16c72917aeed962d3e63d8d4d604ec4692dedfb4a84de2f96 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_32fe0b6fc752eca16c72917aeed962d3e63d8d4d604ec4692dedfb4a84de2f96->enter($__internal_32fe0b6fc752eca16c72917aeed962d3e63d8d4d604ec4692dedfb4a84de2f96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $__internal_3b1ae06171636ad6cfdcfb832e473a47c54257fb993efa0b37c7e44f6277737a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3b1ae06171636ad6cfdcfb832e473a47c54257fb993efa0b37c7e44f6277737a->enter($__internal_3b1ae06171636ad6cfdcfb832e473a47c54257fb993efa0b37c7e44f6277737a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_32fe0b6fc752eca16c72917aeed962d3e63d8d4d604ec4692dedfb4a84de2f96->leave($__internal_32fe0b6fc752eca16c72917aeed962d3e63d8d4d604ec4692dedfb4a84de2f96_prof);

        
        $__internal_3b1ae06171636ad6cfdcfb832e473a47c54257fb993efa0b37c7e44f6277737a->leave($__internal_3b1ae06171636ad6cfdcfb832e473a47c54257fb993efa0b37c7e44f6277737a_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_407108b98b580c737ce4230aa1bef22168623f76c73136a5308668a533ca8731 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_407108b98b580c737ce4230aa1bef22168623f76c73136a5308668a533ca8731->enter($__internal_407108b98b580c737ce4230aa1bef22168623f76c73136a5308668a533ca8731_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_384917ca0d728b1cfd960edbdf30af650e2922e59f6a19090a59343a0b4dab1f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_384917ca0d728b1cfd960edbdf30af650e2922e59f6a19090a59343a0b4dab1f->enter($__internal_384917ca0d728b1cfd960edbdf30af650e2922e59f6a19090a59343a0b4dab1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_384917ca0d728b1cfd960edbdf30af650e2922e59f6a19090a59343a0b4dab1f->leave($__internal_384917ca0d728b1cfd960edbdf30af650e2922e59f6a19090a59343a0b4dab1f_prof);

        
        $__internal_407108b98b580c737ce4230aa1bef22168623f76c73136a5308668a533ca8731->leave($__internal_407108b98b580c737ce4230aa1bef22168623f76c73136a5308668a533ca8731_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:show.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show.html.twig");
    }
}
