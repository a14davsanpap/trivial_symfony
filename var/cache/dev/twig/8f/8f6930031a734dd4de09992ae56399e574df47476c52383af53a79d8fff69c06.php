<?php

/* jugador.html.twig */
class __TwigTemplate_86138ea94c565fa8ffe3a0d9aa3ec991fddc6da766885739c89bad9fe2ed0997 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_38c792bf5f6147e65cfb5e83c843d9f6fdc000d4959a7c98a7a9cf05368999ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_38c792bf5f6147e65cfb5e83c843d9f6fdc000d4959a7c98a7a9cf05368999ba->enter($__internal_38c792bf5f6147e65cfb5e83c843d9f6fdc000d4959a7c98a7a9cf05368999ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "jugador.html.twig"));

        $__internal_3b8164acc05d6157606c20cc2313e43899134fe07283df2b6156b24cb9224cd5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3b8164acc05d6157606c20cc2313e43899134fe07283df2b6156b24cb9224cd5->enter($__internal_3b8164acc05d6157606c20cc2313e43899134fe07283df2b6156b24cb9224cd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "jugador.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <style type=\"text/css\">
        body{
            background-color: #33586A;
        }
\t\t#posicio{
\t\t\ttext-align: center;
\t\t}

\t\t.tauler{
            height: 600px;
            width: 600px;
\t\t}
        
            .titol{
                text-align: center;
            }
            
            #numero{
                border-style: solid;
                width: 70px;
                height: 100px;
                text-align: center;
                margin-top: 10px;
            }
            
            #dau{
                margin-top: 10px;
                
            }
        </style>
    </head>
    <body>
        <h1 class=\"titol\">TRIVIAL</h1>
     
        <div id=\"numero\">
        <a href=\"";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("partida2");
        echo "\"> <img id=\"dau\" src=\"img/dados.png\" width=\"60px\" height=\"60px\"></a>

        ";
        // line 46
        echo twig_escape_filter($this->env, ($context["number"] ?? $this->getContext($context, "number")), "html", null, true);
        echo "
        </div>


    <div id=posicio>
        <img id=\"tauler\" src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/tablero.jpg"), "html", null, true);
        echo "\" alt=\"Symfony!\" />
    </div>
    </body>
</html>";
        
        $__internal_38c792bf5f6147e65cfb5e83c843d9f6fdc000d4959a7c98a7a9cf05368999ba->leave($__internal_38c792bf5f6147e65cfb5e83c843d9f6fdc000d4959a7c98a7a9cf05368999ba_prof);

        
        $__internal_3b8164acc05d6157606c20cc2313e43899134fe07283df2b6156b24cb9224cd5->leave($__internal_3b8164acc05d6157606c20cc2313e43899134fe07283df2b6156b24cb9224cd5_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_61f698072a4c7fe3991a8dd9e06d62edd647a5e140211b88ed8a03ac78393057 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_61f698072a4c7fe3991a8dd9e06d62edd647a5e140211b88ed8a03ac78393057->enter($__internal_61f698072a4c7fe3991a8dd9e06d62edd647a5e140211b88ed8a03ac78393057_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_6d146d9999f9b5140e5424b5b0057204a894da04423ef40a00afd24aae65ebe7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d146d9999f9b5140e5424b5b0057204a894da04423ef40a00afd24aae65ebe7->enter($__internal_6d146d9999f9b5140e5424b5b0057204a894da04423ef40a00afd24aae65ebe7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "TRIVIAL";
        
        $__internal_6d146d9999f9b5140e5424b5b0057204a894da04423ef40a00afd24aae65ebe7->leave($__internal_6d146d9999f9b5140e5424b5b0057204a894da04423ef40a00afd24aae65ebe7_prof);

        
        $__internal_61f698072a4c7fe3991a8dd9e06d62edd647a5e140211b88ed8a03ac78393057->leave($__internal_61f698072a4c7fe3991a8dd9e06d62edd647a5e140211b88ed8a03ac78393057_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_5a5ed158ffd2ac6ae833ae339b72c79026f793bc25af5ae982f531adeee7bce4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a5ed158ffd2ac6ae833ae339b72c79026f793bc25af5ae982f531adeee7bce4->enter($__internal_5a5ed158ffd2ac6ae833ae339b72c79026f793bc25af5ae982f531adeee7bce4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_e34ada6d62851699c18e66780864fd0f9d67e00e3a79f536e84af469872dba0a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e34ada6d62851699c18e66780864fd0f9d67e00e3a79f536e84af469872dba0a->enter($__internal_e34ada6d62851699c18e66780864fd0f9d67e00e3a79f536e84af469872dba0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_e34ada6d62851699c18e66780864fd0f9d67e00e3a79f536e84af469872dba0a->leave($__internal_e34ada6d62851699c18e66780864fd0f9d67e00e3a79f536e84af469872dba0a_prof);

        
        $__internal_5a5ed158ffd2ac6ae833ae339b72c79026f793bc25af5ae982f531adeee7bce4->leave($__internal_5a5ed158ffd2ac6ae833ae339b72c79026f793bc25af5ae982f531adeee7bce4_prof);

    }

    public function getTemplateName()
    {
        return "jugador.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 6,  108 => 5,  94 => 51,  86 => 46,  81 => 44,  42 => 8,  39 => 7,  37 => 6,  33 => 5,  27 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}TRIVIAL{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        <style type=\"text/css\">
        body{
            background-color: #33586A;
        }
\t\t#posicio{
\t\t\ttext-align: center;
\t\t}

\t\t.tauler{
            height: 600px;
            width: 600px;
\t\t}
        
            .titol{
                text-align: center;
            }
            
            #numero{
                border-style: solid;
                width: 70px;
                height: 100px;
                text-align: center;
                margin-top: 10px;
            }
            
            #dau{
                margin-top: 10px;
                
            }
        </style>
    </head>
    <body>
        <h1 class=\"titol\">TRIVIAL</h1>
     
        <div id=\"numero\">
        <a href=\"{{ path('partida2') }}\"> <img id=\"dau\" src=\"img/dados.png\" width=\"60px\" height=\"60px\"></a>

        {{number}}
        </div>


    <div id=posicio>
        <img id=\"tauler\" src=\"{{ asset('img/tablero.jpg') }}\" alt=\"Symfony!\" />
    </div>
    </body>
</html>", "jugador.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/app/Resources/views/jugador.html.twig");
    }
}
