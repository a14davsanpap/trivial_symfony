<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_a3115c01f0f98af2e3fa2fd019f03553964fb6866ffe8d8c011a7e57ec5b13ac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4a204d4d1e70c689825262e733583aa50f19e522ddec3418583151678c61a70e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a204d4d1e70c689825262e733583aa50f19e522ddec3418583151678c61a70e->enter($__internal_4a204d4d1e70c689825262e733583aa50f19e522ddec3418583151678c61a70e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        $__internal_7f5345a66c6c6e1f27925367676f6d8618fb0a262258dbab37bb47a38ccadbfb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7f5345a66c6c6e1f27925367676f6d8618fb0a262258dbab37bb47a38ccadbfb->enter($__internal_7f5345a66c6c6e1f27925367676f6d8618fb0a262258dbab37bb47a38ccadbfb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_4a204d4d1e70c689825262e733583aa50f19e522ddec3418583151678c61a70e->leave($__internal_4a204d4d1e70c689825262e733583aa50f19e522ddec3418583151678c61a70e_prof);

        
        $__internal_7f5345a66c6c6e1f27925367676f6d8618fb0a262258dbab37bb47a38ccadbfb->leave($__internal_7f5345a66c6c6e1f27925367676f6d8618fb0a262258dbab37bb47a38ccadbfb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form.html.php");
    }
}
