<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_43cfa4df6fa507a07dd290b4b81fca69b70f03cab0b64e18100a287cfa1b70c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_279db57cd46a813f25ba73ef547b5a1bc9d0c6643a54cd5e414667a542e9df8e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_279db57cd46a813f25ba73ef547b5a1bc9d0c6643a54cd5e414667a542e9df8e->enter($__internal_279db57cd46a813f25ba73ef547b5a1bc9d0c6643a54cd5e414667a542e9df8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_251a68c21a38bb3eef2ba2fd19617d165b7060de89b8a05e4a7eba1dc241883d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_251a68c21a38bb3eef2ba2fd19617d165b7060de89b8a05e4a7eba1dc241883d->enter($__internal_251a68c21a38bb3eef2ba2fd19617d165b7060de89b8a05e4a7eba1dc241883d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_279db57cd46a813f25ba73ef547b5a1bc9d0c6643a54cd5e414667a542e9df8e->leave($__internal_279db57cd46a813f25ba73ef547b5a1bc9d0c6643a54cd5e414667a542e9df8e_prof);

        
        $__internal_251a68c21a38bb3eef2ba2fd19617d165b7060de89b8a05e4a7eba1dc241883d->leave($__internal_251a68c21a38bb3eef2ba2fd19617d165b7060de89b8a05e4a7eba1dc241883d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}
