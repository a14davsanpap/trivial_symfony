<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_130f1aae5fb1f2edca868dca51bc325092dc39c0a8ac85fe6a9574f7ab9718a7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8a27200c282514eb2fd310e7497857646e50721ac4ad84047ecce615b730caa5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8a27200c282514eb2fd310e7497857646e50721ac4ad84047ecce615b730caa5->enter($__internal_8a27200c282514eb2fd310e7497857646e50721ac4ad84047ecce615b730caa5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_4374e42e8193c388c9f86736e33c806b5fe2e15c9d62d39a50ea06f251f23416 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4374e42e8193c388c9f86736e33c806b5fe2e15c9d62d39a50ea06f251f23416->enter($__internal_4374e42e8193c388c9f86736e33c806b5fe2e15c9d62d39a50ea06f251f23416_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_8a27200c282514eb2fd310e7497857646e50721ac4ad84047ecce615b730caa5->leave($__internal_8a27200c282514eb2fd310e7497857646e50721ac4ad84047ecce615b730caa5_prof);

        
        $__internal_4374e42e8193c388c9f86736e33c806b5fe2e15c9d62d39a50ea06f251f23416->leave($__internal_4374e42e8193c388c9f86736e33c806b5fe2e15c9d62d39a50ea06f251f23416_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_row.html.php");
    }
}
