<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_285361a739a8e127b8c03d3a499dd21b25f1195b00a89a560601c42fb5ee6a98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e332c545d5a640e1b2584d16796747cd05cc7343db10055c868af80711822b9c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e332c545d5a640e1b2584d16796747cd05cc7343db10055c868af80711822b9c->enter($__internal_e332c545d5a640e1b2584d16796747cd05cc7343db10055c868af80711822b9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $__internal_cdc4302df1abce8ed67df44e181c95210243df4487caabddbe2eb072b455ed40 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cdc4302df1abce8ed67df44e181c95210243df4487caabddbe2eb072b455ed40->enter($__internal_cdc4302df1abce8ed67df44e181c95210243df4487caabddbe2eb072b455ed40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e332c545d5a640e1b2584d16796747cd05cc7343db10055c868af80711822b9c->leave($__internal_e332c545d5a640e1b2584d16796747cd05cc7343db10055c868af80711822b9c_prof);

        
        $__internal_cdc4302df1abce8ed67df44e181c95210243df4487caabddbe2eb072b455ed40->leave($__internal_cdc4302df1abce8ed67df44e181c95210243df4487caabddbe2eb072b455ed40_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ccb0ea8b6e8e7c34c267b4b76cf3a0768137a36a2eeb5b2b7447f5255f4a5153 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ccb0ea8b6e8e7c34c267b4b76cf3a0768137a36a2eeb5b2b7447f5255f4a5153->enter($__internal_ccb0ea8b6e8e7c34c267b4b76cf3a0768137a36a2eeb5b2b7447f5255f4a5153_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_2733b090686cf54d075f082c4f8e3be8fd41a93fed1d416fc3b263348e4b148c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2733b090686cf54d075f082c4f8e3be8fd41a93fed1d416fc3b263348e4b148c->enter($__internal_2733b090686cf54d075f082c4f8e3be8fd41a93fed1d416fc3b263348e4b148c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_2733b090686cf54d075f082c4f8e3be8fd41a93fed1d416fc3b263348e4b148c->leave($__internal_2733b090686cf54d075f082c4f8e3be8fd41a93fed1d416fc3b263348e4b148c_prof);

        
        $__internal_ccb0ea8b6e8e7c34c267b4b76cf3a0768137a36a2eeb5b2b7447f5255f4a5153->leave($__internal_ccb0ea8b6e8e7c34c267b4b76cf3a0768137a36a2eeb5b2b7447f5255f4a5153_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/new_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:new.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new.html.twig");
    }
}
