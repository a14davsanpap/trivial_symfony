<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_c6f06d42be8bf90389e58359ef7b6d90c87b52f4126969fecd20e7ec7bc4287d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_949745a548d936853b819286921d77c948a7e7b069126762d0bb0baa463e2268 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_949745a548d936853b819286921d77c948a7e7b069126762d0bb0baa463e2268->enter($__internal_949745a548d936853b819286921d77c948a7e7b069126762d0bb0baa463e2268_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_785c7ae8bac895c3f8ed64a1800272c593bd698b9d36e3ad69a721c795d850ae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_785c7ae8bac895c3f8ed64a1800272c593bd698b9d36e3ad69a721c795d850ae->enter($__internal_785c7ae8bac895c3f8ed64a1800272c593bd698b9d36e3ad69a721c795d850ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_949745a548d936853b819286921d77c948a7e7b069126762d0bb0baa463e2268->leave($__internal_949745a548d936853b819286921d77c948a7e7b069126762d0bb0baa463e2268_prof);

        
        $__internal_785c7ae8bac895c3f8ed64a1800272c593bd698b9d36e3ad69a721c795d850ae->leave($__internal_785c7ae8bac895c3f8ed64a1800272c593bd698b9d36e3ad69a721c795d850ae_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}
