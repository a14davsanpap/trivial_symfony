<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_0bebe7c164fe3ebbaa2bce1e7f6ccfc3da01ccfd2c6405be9584e3ab64fed520 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6e05922d6c37556812653080137519c69a7c24e9da0b0c21a5ea3ba36ee5b7ed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6e05922d6c37556812653080137519c69a7c24e9da0b0c21a5ea3ba36ee5b7ed->enter($__internal_6e05922d6c37556812653080137519c69a7c24e9da0b0c21a5ea3ba36ee5b7ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $__internal_eea3413865397fdbc4b345a2e2a1642d26b37fe237df6396187233976c66f6de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eea3413865397fdbc4b345a2e2a1642d26b37fe237df6396187233976c66f6de->enter($__internal_eea3413865397fdbc4b345a2e2a1642d26b37fe237df6396187233976c66f6de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6e05922d6c37556812653080137519c69a7c24e9da0b0c21a5ea3ba36ee5b7ed->leave($__internal_6e05922d6c37556812653080137519c69a7c24e9da0b0c21a5ea3ba36ee5b7ed_prof);

        
        $__internal_eea3413865397fdbc4b345a2e2a1642d26b37fe237df6396187233976c66f6de->leave($__internal_eea3413865397fdbc4b345a2e2a1642d26b37fe237df6396187233976c66f6de_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_21a56f4283e773c6ccad8baaa08ec0469cb0380e9d29d74127fbe3743e1f6ba9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_21a56f4283e773c6ccad8baaa08ec0469cb0380e9d29d74127fbe3743e1f6ba9->enter($__internal_21a56f4283e773c6ccad8baaa08ec0469cb0380e9d29d74127fbe3743e1f6ba9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_0ee914053579abc896ead065fe16fecb0f557b8a91d1a258b86ab3a03265cc37 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0ee914053579abc896ead065fe16fecb0f557b8a91d1a258b86ab3a03265cc37->enter($__internal_0ee914053579abc896ead065fe16fecb0f557b8a91d1a258b86ab3a03265cc37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_0ee914053579abc896ead065fe16fecb0f557b8a91d1a258b86ab3a03265cc37->leave($__internal_0ee914053579abc896ead065fe16fecb0f557b8a91d1a258b86ab3a03265cc37_prof);

        
        $__internal_21a56f4283e773c6ccad8baaa08ec0469cb0380e9d29d74127fbe3743e1f6ba9->leave($__internal_21a56f4283e773c6ccad8baaa08ec0469cb0380e9d29d74127fbe3743e1f6ba9_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:show.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show.html.twig");
    }
}
