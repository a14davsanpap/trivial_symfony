<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_150ca7d995468e02789c19d70f5a82365301ecebc21b38fde8bc2214e9f04740 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5c968e81ba692f52f972d70a374f9a2934ebfa8e6f2b1016ec1defbeba160d06 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5c968e81ba692f52f972d70a374f9a2934ebfa8e6f2b1016ec1defbeba160d06->enter($__internal_5c968e81ba692f52f972d70a374f9a2934ebfa8e6f2b1016ec1defbeba160d06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_1d6e8de8977294a269905840422a3109813d1015da2481a271312b3e11a14f9e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d6e8de8977294a269905840422a3109813d1015da2481a271312b3e11a14f9e->enter($__internal_1d6e8de8977294a269905840422a3109813d1015da2481a271312b3e11a14f9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5c968e81ba692f52f972d70a374f9a2934ebfa8e6f2b1016ec1defbeba160d06->leave($__internal_5c968e81ba692f52f972d70a374f9a2934ebfa8e6f2b1016ec1defbeba160d06_prof);

        
        $__internal_1d6e8de8977294a269905840422a3109813d1015da2481a271312b3e11a14f9e->leave($__internal_1d6e8de8977294a269905840422a3109813d1015da2481a271312b3e11a14f9e_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_2b3a4868fb198f7a0ded86fb5f74bf81e31f5c7460e1635158ed6406a5cbc174 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b3a4868fb198f7a0ded86fb5f74bf81e31f5c7460e1635158ed6406a5cbc174->enter($__internal_2b3a4868fb198f7a0ded86fb5f74bf81e31f5c7460e1635158ed6406a5cbc174_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_bcf749dde251a8d77b993173fab9d6f244cb27b97394fc375a9919350c165813 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bcf749dde251a8d77b993173fab9d6f244cb27b97394fc375a9919350c165813->enter($__internal_bcf749dde251a8d77b993173fab9d6f244cb27b97394fc375a9919350c165813_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_bcf749dde251a8d77b993173fab9d6f244cb27b97394fc375a9919350c165813->leave($__internal_bcf749dde251a8d77b993173fab9d6f244cb27b97394fc375a9919350c165813_prof);

        
        $__internal_2b3a4868fb198f7a0ded86fb5f74bf81e31f5c7460e1635158ed6406a5cbc174->leave($__internal_2b3a4868fb198f7a0ded86fb5f74bf81e31f5c7460e1635158ed6406a5cbc174_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_8eb86a6904a4710972348be9be6d8c421263f3b7818ca861037eabb200186c76 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8eb86a6904a4710972348be9be6d8c421263f3b7818ca861037eabb200186c76->enter($__internal_8eb86a6904a4710972348be9be6d8c421263f3b7818ca861037eabb200186c76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_500ec832431d2f4ffeffb435dca6483325d60be7e00d7d21d00675d37cb8deb1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_500ec832431d2f4ffeffb435dca6483325d60be7e00d7d21d00675d37cb8deb1->enter($__internal_500ec832431d2f4ffeffb435dca6483325d60be7e00d7d21d00675d37cb8deb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_500ec832431d2f4ffeffb435dca6483325d60be7e00d7d21d00675d37cb8deb1->leave($__internal_500ec832431d2f4ffeffb435dca6483325d60be7e00d7d21d00675d37cb8deb1_prof);

        
        $__internal_8eb86a6904a4710972348be9be6d8c421263f3b7818ca861037eabb200186c76->leave($__internal_8eb86a6904a4710972348be9be6d8c421263f3b7818ca861037eabb200186c76_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
