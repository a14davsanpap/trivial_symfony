<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_5dd881581bc15836809338988f2ffb1d30def9304e678fa262239ebe510c712a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f75ceb8d3730c37db44db6c4cfcf95a41c265790472b0bbcc86675122c6da1c3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f75ceb8d3730c37db44db6c4cfcf95a41c265790472b0bbcc86675122c6da1c3->enter($__internal_f75ceb8d3730c37db44db6c4cfcf95a41c265790472b0bbcc86675122c6da1c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_984097fe84f63ef65b56f71ccb15611e88531e6b6e514341a4cc4f4ea00d02f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_984097fe84f63ef65b56f71ccb15611e88531e6b6e514341a4cc4f4ea00d02f6->enter($__internal_984097fe84f63ef65b56f71ccb15611e88531e6b6e514341a4cc4f4ea00d02f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_f75ceb8d3730c37db44db6c4cfcf95a41c265790472b0bbcc86675122c6da1c3->leave($__internal_f75ceb8d3730c37db44db6c4cfcf95a41c265790472b0bbcc86675122c6da1c3_prof);

        
        $__internal_984097fe84f63ef65b56f71ccb15611e88531e6b6e514341a4cc4f4ea00d02f6->leave($__internal_984097fe84f63ef65b56f71ccb15611e88531e6b6e514341a4cc4f4ea00d02f6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/percent_widget.html.php");
    }
}
