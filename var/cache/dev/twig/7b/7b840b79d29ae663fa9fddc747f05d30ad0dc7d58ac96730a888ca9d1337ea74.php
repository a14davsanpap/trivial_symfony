<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_aa15f58eb5b23ff33096a330603351099836f7a58ca0137ee1746301f0e7d28e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_47c17f484d643e1d1d6caa374d8452ce627c0f5024f33d94d511308d496fcb05 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_47c17f484d643e1d1d6caa374d8452ce627c0f5024f33d94d511308d496fcb05->enter($__internal_47c17f484d643e1d1d6caa374d8452ce627c0f5024f33d94d511308d496fcb05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_785aa96c37fdc40bbe4ed2e634c5748537e7da5a84b6202441f011c1f533612c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_785aa96c37fdc40bbe4ed2e634c5748537e7da5a84b6202441f011c1f533612c->enter($__internal_785aa96c37fdc40bbe4ed2e634c5748537e7da5a84b6202441f011c1f533612c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_47c17f484d643e1d1d6caa374d8452ce627c0f5024f33d94d511308d496fcb05->leave($__internal_47c17f484d643e1d1d6caa374d8452ce627c0f5024f33d94d511308d496fcb05_prof);

        
        $__internal_785aa96c37fdc40bbe4ed2e634c5748537e7da5a84b6202441f011c1f533612c->leave($__internal_785aa96c37fdc40bbe4ed2e634c5748537e7da5a84b6202441f011c1f533612c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/collection_widget.html.php");
    }
}
