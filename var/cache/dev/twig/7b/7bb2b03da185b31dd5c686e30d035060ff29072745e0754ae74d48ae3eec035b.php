<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_2860e5c2f8c997d9d0dcb582a6c488d9a51615957c5bde6e68a1c68889f2393e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_519a74c9b2c37a6aca3955dfa9e6eb5d717d9177531c1cd30cf2119cc2d23917 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_519a74c9b2c37a6aca3955dfa9e6eb5d717d9177531c1cd30cf2119cc2d23917->enter($__internal_519a74c9b2c37a6aca3955dfa9e6eb5d717d9177531c1cd30cf2119cc2d23917_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_35ac5641ff195004f3e11c9c762e88b66e48ac5e3d9cd2fdaeee011f5565e3dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_35ac5641ff195004f3e11c9c762e88b66e48ac5e3d9cd2fdaeee011f5565e3dc->enter($__internal_35ac5641ff195004f3e11c9c762e88b66e48ac5e3d9cd2fdaeee011f5565e3dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_519a74c9b2c37a6aca3955dfa9e6eb5d717d9177531c1cd30cf2119cc2d23917->leave($__internal_519a74c9b2c37a6aca3955dfa9e6eb5d717d9177531c1cd30cf2119cc2d23917_prof);

        
        $__internal_35ac5641ff195004f3e11c9c762e88b66e48ac5e3d9cd2fdaeee011f5565e3dc->leave($__internal_35ac5641ff195004f3e11c9c762e88b66e48ac5e3d9cd2fdaeee011f5565e3dc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/textarea_widget.html.php");
    }
}
