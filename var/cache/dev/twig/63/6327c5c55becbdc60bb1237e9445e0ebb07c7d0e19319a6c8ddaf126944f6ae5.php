<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_3778406ef85a6637d2a2a0be9d378272396e22464ec59695638d364213c2a4f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e9fc877b0a02066e6980db7535cb6e42be1b63028314387f80e462b9a9f67f5c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e9fc877b0a02066e6980db7535cb6e42be1b63028314387f80e462b9a9f67f5c->enter($__internal_e9fc877b0a02066e6980db7535cb6e42be1b63028314387f80e462b9a9f67f5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_f6e35b7b538b9bcf4ab68c6522d32aa820eb707451d51f56f96f6f72a33ab1d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f6e35b7b538b9bcf4ab68c6522d32aa820eb707451d51f56f96f6f72a33ab1d7->enter($__internal_f6e35b7b538b9bcf4ab68c6522d32aa820eb707451d51f56f96f6f72a33ab1d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_e9fc877b0a02066e6980db7535cb6e42be1b63028314387f80e462b9a9f67f5c->leave($__internal_e9fc877b0a02066e6980db7535cb6e42be1b63028314387f80e462b9a9f67f5c_prof);

        
        $__internal_f6e35b7b538b9bcf4ab68c6522d32aa820eb707451d51f56f96f6f72a33ab1d7->leave($__internal_f6e35b7b538b9bcf4ab68c6522d32aa820eb707451d51f56f96f6f72a33ab1d7_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_6d83a26f1200f460cd2d4cdb3c4197cc83c72844f447dcb7729b17c479d51559 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6d83a26f1200f460cd2d4cdb3c4197cc83c72844f447dcb7729b17c479d51559->enter($__internal_6d83a26f1200f460cd2d4cdb3c4197cc83c72844f447dcb7729b17c479d51559_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_d393db2aa52c0fff0b35dc053d127a9dc12563b1ffce2009401fc14c931a54b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d393db2aa52c0fff0b35dc053d127a9dc12563b1ffce2009401fc14c931a54b9->enter($__internal_d393db2aa52c0fff0b35dc053d127a9dc12563b1ffce2009401fc14c931a54b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_d393db2aa52c0fff0b35dc053d127a9dc12563b1ffce2009401fc14c931a54b9->leave($__internal_d393db2aa52c0fff0b35dc053d127a9dc12563b1ffce2009401fc14c931a54b9_prof);

        
        $__internal_6d83a26f1200f460cd2d4cdb3c4197cc83c72844f447dcb7729b17c479d51559->leave($__internal_6d83a26f1200f460cd2d4cdb3c4197cc83c72844f447dcb7729b17c479d51559_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
