<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_d74d8c725eac15e4fb152042b00cc910d59d819fe0c6a4d9c3d533751f8e7ea1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fbd69c202d160dcf4ee360abe1e006f661685b65120e4d482f1c60f5a695c47a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fbd69c202d160dcf4ee360abe1e006f661685b65120e4d482f1c60f5a695c47a->enter($__internal_fbd69c202d160dcf4ee360abe1e006f661685b65120e4d482f1c60f5a695c47a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        $__internal_081453dc020033d0e2f39cd8f75d1529eb7800b6fc007c61ec479fdb7fc64766 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_081453dc020033d0e2f39cd8f75d1529eb7800b6fc007c61ec479fdb7fc64766->enter($__internal_081453dc020033d0e2f39cd8f75d1529eb7800b6fc007c61ec479fdb7fc64766_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_fbd69c202d160dcf4ee360abe1e006f661685b65120e4d482f1c60f5a695c47a->leave($__internal_fbd69c202d160dcf4ee360abe1e006f661685b65120e4d482f1c60f5a695c47a_prof);

        
        $__internal_081453dc020033d0e2f39cd8f75d1529eb7800b6fc007c61ec479fdb7fc64766->leave($__internal_081453dc020033d0e2f39cd8f75d1529eb7800b6fc007c61ec479fdb7fc64766_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_14ce203d852a479dbac5fc2f59d3871430780d8496e12aabd869b6bbe8005932 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_14ce203d852a479dbac5fc2f59d3871430780d8496e12aabd869b6bbe8005932->enter($__internal_14ce203d852a479dbac5fc2f59d3871430780d8496e12aabd869b6bbe8005932_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_0bb801a1288983ded45ca0097085322f4ecaf9756a50dbe8aecb55f05d2b693a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0bb801a1288983ded45ca0097085322f4ecaf9756a50dbe8aecb55f05d2b693a->enter($__internal_0bb801a1288983ded45ca0097085322f4ecaf9756a50dbe8aecb55f05d2b693a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        
        $__internal_0bb801a1288983ded45ca0097085322f4ecaf9756a50dbe8aecb55f05d2b693a->leave($__internal_0bb801a1288983ded45ca0097085322f4ecaf9756a50dbe8aecb55f05d2b693a_prof);

        
        $__internal_14ce203d852a479dbac5fc2f59d3871430780d8496e12aabd869b6bbe8005932->leave($__internal_14ce203d852a479dbac5fc2f59d3871430780d8496e12aabd869b6bbe8005932_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_c5f5d7f8024e64e7f7e918fc8c0fc75caa5f838ab6b8fc78b14fd34ac636d285 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c5f5d7f8024e64e7f7e918fc8c0fc75caa5f838ab6b8fc78b14fd34ac636d285->enter($__internal_c5f5d7f8024e64e7f7e918fc8c0fc75caa5f838ab6b8fc78b14fd34ac636d285_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_62490aaae5542a202358507d607d1dc05729321bb1c728e9829f14ff437238b7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_62490aaae5542a202358507d607d1dc05729321bb1c728e9829f14ff437238b7->enter($__internal_62490aaae5542a202358507d607d1dc05729321bb1c728e9829f14ff437238b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_62490aaae5542a202358507d607d1dc05729321bb1c728e9829f14ff437238b7->leave($__internal_62490aaae5542a202358507d607d1dc05729321bb1c728e9829f14ff437238b7_prof);

        
        $__internal_c5f5d7f8024e64e7f7e918fc8c0fc75caa5f838ab6b8fc78b14fd34ac636d285->leave($__internal_c5f5d7f8024e64e7f7e918fc8c0fc75caa5f838ab6b8fc78b14fd34ac636d285_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_979d58684044edb02e5ef295497ff255050d66c1dc00f225b33d450195df32b8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_979d58684044edb02e5ef295497ff255050d66c1dc00f225b33d450195df32b8->enter($__internal_979d58684044edb02e5ef295497ff255050d66c1dc00f225b33d450195df32b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_206cc2948d7f634e1a9919a64757e2f3ce30b7859fbae7719abb7c210dcbe488 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_206cc2948d7f634e1a9919a64757e2f3ce30b7859fbae7719abb7c210dcbe488->enter($__internal_206cc2948d7f634e1a9919a64757e2f3ce30b7859fbae7719abb7c210dcbe488_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_206cc2948d7f634e1a9919a64757e2f3ce30b7859fbae7719abb7c210dcbe488->leave($__internal_206cc2948d7f634e1a9919a64757e2f3ce30b7859fbae7719abb7c210dcbe488_prof);

        
        $__internal_979d58684044edb02e5ef295497ff255050d66c1dc00f225b33d450195df32b8->leave($__internal_979d58684044edb02e5ef295497ff255050d66c1dc00f225b33d450195df32b8_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Resetting:email.txt.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/email.txt.twig");
    }
}
