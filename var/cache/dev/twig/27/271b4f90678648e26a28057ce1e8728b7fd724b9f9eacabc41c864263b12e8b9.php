<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_59d5d23cf80985ca05cf070524c716364a72aca249f7b2742bfcaa8407392ad0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c6c2c6d5400a01e14a1c9f6463bb2bbb3bcab795e0b64d06eefd594454db936b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c6c2c6d5400a01e14a1c9f6463bb2bbb3bcab795e0b64d06eefd594454db936b->enter($__internal_c6c2c6d5400a01e14a1c9f6463bb2bbb3bcab795e0b64d06eefd594454db936b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $__internal_9ab178e8affdb7bb22e78f97aa058df7c9c63aa14bcece7aee19a9bfa800948c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ab178e8affdb7bb22e78f97aa058df7c9c63aa14bcece7aee19a9bfa800948c->enter($__internal_9ab178e8affdb7bb22e78f97aa058df7c9c63aa14bcece7aee19a9bfa800948c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c6c2c6d5400a01e14a1c9f6463bb2bbb3bcab795e0b64d06eefd594454db936b->leave($__internal_c6c2c6d5400a01e14a1c9f6463bb2bbb3bcab795e0b64d06eefd594454db936b_prof);

        
        $__internal_9ab178e8affdb7bb22e78f97aa058df7c9c63aa14bcece7aee19a9bfa800948c->leave($__internal_9ab178e8affdb7bb22e78f97aa058df7c9c63aa14bcece7aee19a9bfa800948c_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_43f3131d321fbad78ed44646d432cc308fc63255fba4618c84e5b97502b77874 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_43f3131d321fbad78ed44646d432cc308fc63255fba4618c84e5b97502b77874->enter($__internal_43f3131d321fbad78ed44646d432cc308fc63255fba4618c84e5b97502b77874_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_2419b4ac6c6f3e2be0fab87d5c240f504635bfbb0c62b5c1e36276280cea350e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2419b4ac6c6f3e2be0fab87d5c240f504635bfbb0c62b5c1e36276280cea350e->enter($__internal_2419b4ac6c6f3e2be0fab87d5c240f504635bfbb0c62b5c1e36276280cea350e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_2419b4ac6c6f3e2be0fab87d5c240f504635bfbb0c62b5c1e36276280cea350e->leave($__internal_2419b4ac6c6f3e2be0fab87d5c240f504635bfbb0c62b5c1e36276280cea350e_prof);

        
        $__internal_43f3131d321fbad78ed44646d432cc308fc63255fba4618c84e5b97502b77874->leave($__internal_43f3131d321fbad78ed44646d432cc308fc63255fba4618c84e5b97502b77874_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/request_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:request.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/request.html.twig");
    }
}
