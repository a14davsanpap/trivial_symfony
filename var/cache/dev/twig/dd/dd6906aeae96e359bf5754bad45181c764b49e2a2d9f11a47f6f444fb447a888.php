<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_9553fa96834c0ab4dcb8de3e70f72131636b26386dd26bde3b020531765d74b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cd25318c9329ed0e2aaafb891223a5e9742dd5bf86d3c9b2c1c53ccd7b67b293 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cd25318c9329ed0e2aaafb891223a5e9742dd5bf86d3c9b2c1c53ccd7b67b293->enter($__internal_cd25318c9329ed0e2aaafb891223a5e9742dd5bf86d3c9b2c1c53ccd7b67b293_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_a04f241a783c2d9081d635fddf0f5a53164e72a77ff67138a1d67e60ebefa1e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a04f241a783c2d9081d635fddf0f5a53164e72a77ff67138a1d67e60ebefa1e6->enter($__internal_a04f241a783c2d9081d635fddf0f5a53164e72a77ff67138a1d67e60ebefa1e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_cd25318c9329ed0e2aaafb891223a5e9742dd5bf86d3c9b2c1c53ccd7b67b293->leave($__internal_cd25318c9329ed0e2aaafb891223a5e9742dd5bf86d3c9b2c1c53ccd7b67b293_prof);

        
        $__internal_a04f241a783c2d9081d635fddf0f5a53164e72a77ff67138a1d67e60ebefa1e6->leave($__internal_a04f241a783c2d9081d635fddf0f5a53164e72a77ff67138a1d67e60ebefa1e6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}
