<?php

/* opcions.html.twig */
class __TwigTemplate_9dfe4edc7172a96034da7a57c3de218ebc9b1c8def3043dddcf522c978900caf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f9fd2ad397f7fbe2690f5e191984305e0fbbd5501c8059c7ec465fd9f1e50478 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f9fd2ad397f7fbe2690f5e191984305e0fbbd5501c8059c7ec465fd9f1e50478->enter($__internal_f9fd2ad397f7fbe2690f5e191984305e0fbbd5501c8059c7ec465fd9f1e50478_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "opcions.html.twig"));

        $__internal_829e671e7134e851bd7cb88beab1c298e3143875b30f88d3a19ca9f26fa2d1dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_829e671e7134e851bd7cb88beab1c298e3143875b30f88d3a19ca9f26fa2d1dc->enter($__internal_829e671e7134e851bd7cb88beab1c298e3143875b30f88d3a19ca9f26fa2d1dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "opcions.html.twig"));

        // line 1
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/login.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" href=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/opcions.css"), "html", null, true);
        echo "\" />
<html>
    <head>
        
        <style>
            a{
                text-decoration:none;
            }
        </style>
    
    </head>
    
    <body>
        <div class=\"iniciada button\">
            <a href=\"/partida\">  Partida iniciada </a>
        </div>
        
        <div class=\"nova button\">
            <a href=\"/partida\">Nova partida</a>
        </div>
    </body>
</html>";
        
        $__internal_f9fd2ad397f7fbe2690f5e191984305e0fbbd5501c8059c7ec465fd9f1e50478->leave($__internal_f9fd2ad397f7fbe2690f5e191984305e0fbbd5501c8059c7ec465fd9f1e50478_prof);

        
        $__internal_829e671e7134e851bd7cb88beab1c298e3143875b30f88d3a19ca9f26fa2d1dc->leave($__internal_829e671e7134e851bd7cb88beab1c298e3143875b30f88d3a19ca9f26fa2d1dc_prof);

    }

    public function getTemplateName()
    {
        return "opcions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<link rel=\"stylesheet\" href=\"{{ asset('css/login.css') }}\" />
<link rel=\"stylesheet\" href=\"{{ asset('css/opcions.css') }}\" />
<html>
    <head>
        
        <style>
            a{
                text-decoration:none;
            }
        </style>
    
    </head>
    
    <body>
        <div class=\"iniciada button\">
            <a href=\"/partida\">  Partida iniciada </a>
        </div>
        
        <div class=\"nova button\">
            <a href=\"/partida\">Nova partida</a>
        </div>
    </body>
</html>", "opcions.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/app/Resources/views/opcions.html.twig");
    }
}
