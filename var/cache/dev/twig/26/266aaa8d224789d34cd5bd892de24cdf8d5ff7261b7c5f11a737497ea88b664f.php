<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_928c700f1bd55e7d3ce8c237ede9f16eb961626e4bf9ade239529ad63438dae4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6b82bcc720616d3268db89823ec78be25a4a716993ffcc2d73ddb6fdcfd593fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6b82bcc720616d3268db89823ec78be25a4a716993ffcc2d73ddb6fdcfd593fe->enter($__internal_6b82bcc720616d3268db89823ec78be25a4a716993ffcc2d73ddb6fdcfd593fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_bed031c367783e2a6aa78460a0e90f1c320d0f57efba277185ed078571815a6c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bed031c367783e2a6aa78460a0e90f1c320d0f57efba277185ed078571815a6c->enter($__internal_bed031c367783e2a6aa78460a0e90f1c320d0f57efba277185ed078571815a6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_6b82bcc720616d3268db89823ec78be25a4a716993ffcc2d73ddb6fdcfd593fe->leave($__internal_6b82bcc720616d3268db89823ec78be25a4a716993ffcc2d73ddb6fdcfd593fe_prof);

        
        $__internal_bed031c367783e2a6aa78460a0e90f1c320d0f57efba277185ed078571815a6c->leave($__internal_bed031c367783e2a6aa78460a0e90f1c320d0f57efba277185ed078571815a6c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/password_widget.html.php");
    }
}
