<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_20bcc801f96dfdc7fcf00788124282b86fb7c6d7a15ee8d97c1b4d5ad6b52b2b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_493b492b2140a7fe58ea858256420cce90fc5b002262e0b581f317b16dd6e49d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_493b492b2140a7fe58ea858256420cce90fc5b002262e0b581f317b16dd6e49d->enter($__internal_493b492b2140a7fe58ea858256420cce90fc5b002262e0b581f317b16dd6e49d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $__internal_3ddc8bec378d57a98bfb29b7631d2780323ab8be31c533edc98c224f297f26a9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ddc8bec378d57a98bfb29b7631d2780323ab8be31c533edc98c224f297f26a9->enter($__internal_3ddc8bec378d57a98bfb29b7631d2780323ab8be31c533edc98c224f297f26a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_493b492b2140a7fe58ea858256420cce90fc5b002262e0b581f317b16dd6e49d->leave($__internal_493b492b2140a7fe58ea858256420cce90fc5b002262e0b581f317b16dd6e49d_prof);

        
        $__internal_3ddc8bec378d57a98bfb29b7631d2780323ab8be31c533edc98c224f297f26a9->leave($__internal_3ddc8bec378d57a98bfb29b7631d2780323ab8be31c533edc98c224f297f26a9_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_9eaae5f8e7639445720bb5486dcd686738f7b14e8458fac6966d757122228ab0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9eaae5f8e7639445720bb5486dcd686738f7b14e8458fac6966d757122228ab0->enter($__internal_9eaae5f8e7639445720bb5486dcd686738f7b14e8458fac6966d757122228ab0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_921c2afd14110cffda2c539ea957e2776c85f1b414c0563fbccca99c2a47cf2a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_921c2afd14110cffda2c539ea957e2776c85f1b414c0563fbccca99c2a47cf2a->enter($__internal_921c2afd14110cffda2c539ea957e2776c85f1b414c0563fbccca99c2a47cf2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_921c2afd14110cffda2c539ea957e2776c85f1b414c0563fbccca99c2a47cf2a->leave($__internal_921c2afd14110cffda2c539ea957e2776c85f1b414c0563fbccca99c2a47cf2a_prof);

        
        $__internal_9eaae5f8e7639445720bb5486dcd686738f7b14e8458fac6966d757122228ab0->leave($__internal_9eaae5f8e7639445720bb5486dcd686738f7b14e8458fac6966d757122228ab0_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:ChangePassword:change_password.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/friendsofsymfony/user-bundle/Resources/views/ChangePassword/change_password.html.twig");
    }
}
