<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_4637bdf881b03616502cff663e952e1de11fb13ee777e5412be7d6c409e8894f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0321e97c5eb7128bd4daad9b9ab9d6142fbf2041810c6fa74101e1390aa7db02 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0321e97c5eb7128bd4daad9b9ab9d6142fbf2041810c6fa74101e1390aa7db02->enter($__internal_0321e97c5eb7128bd4daad9b9ab9d6142fbf2041810c6fa74101e1390aa7db02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_0f8e4f0e513e04d3d2803ca57b877b4943ec78b737b0b45148994eee159db161 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0f8e4f0e513e04d3d2803ca57b877b4943ec78b737b0b45148994eee159db161->enter($__internal_0f8e4f0e513e04d3d2803ca57b877b4943ec78b737b0b45148994eee159db161_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_0321e97c5eb7128bd4daad9b9ab9d6142fbf2041810c6fa74101e1390aa7db02->leave($__internal_0321e97c5eb7128bd4daad9b9ab9d6142fbf2041810c6fa74101e1390aa7db02_prof);

        
        $__internal_0f8e4f0e513e04d3d2803ca57b877b4943ec78b737b0b45148994eee159db161->leave($__internal_0f8e4f0e513e04d3d2803ca57b877b4943ec78b737b0b45148994eee159db161_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}
