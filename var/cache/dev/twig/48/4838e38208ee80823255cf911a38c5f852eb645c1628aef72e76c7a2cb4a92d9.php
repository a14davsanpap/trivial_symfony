<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_8a72aa64d0b831e00b010149ab4ce976eb1e3850427d11304701fa2c6628539c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_64dda76ee7081ff18abd2a3cc312555f15568aa8650242be406cafec4114ef08 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_64dda76ee7081ff18abd2a3cc312555f15568aa8650242be406cafec4114ef08->enter($__internal_64dda76ee7081ff18abd2a3cc312555f15568aa8650242be406cafec4114ef08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        $__internal_609cfb500f08625560a4e427a795776f7c9e24c82c89062215ca0453b4403020 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_609cfb500f08625560a4e427a795776f7c9e24c82c89062215ca0453b4403020->enter($__internal_609cfb500f08625560a4e427a795776f7c9e24c82c89062215ca0453b4403020_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_64dda76ee7081ff18abd2a3cc312555f15568aa8650242be406cafec4114ef08->leave($__internal_64dda76ee7081ff18abd2a3cc312555f15568aa8650242be406cafec4114ef08_prof);

        
        $__internal_609cfb500f08625560a4e427a795776f7c9e24c82c89062215ca0453b4403020->leave($__internal_609cfb500f08625560a4e427a795776f7c9e24c82c89062215ca0453b4403020_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
", "@Framework/Form/attributes.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/attributes.html.php");
    }
}
