<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_f902452bd4e2b5bd61051f47431cb025fc906d0603e67cac5f83a0eb7cfdef60 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3b90836b44dd67d282e24a14fb4aa0c449624b4b56cb1aaafd4b4aab9245a17a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3b90836b44dd67d282e24a14fb4aa0c449624b4b56cb1aaafd4b4aab9245a17a->enter($__internal_3b90836b44dd67d282e24a14fb4aa0c449624b4b56cb1aaafd4b4aab9245a17a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_dcbfe2ec6537743096d8243037fc15e343d36f7bf4b3adefcfc0282c05a0e03c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dcbfe2ec6537743096d8243037fc15e343d36f7bf4b3adefcfc0282c05a0e03c->enter($__internal_dcbfe2ec6537743096d8243037fc15e343d36f7bf4b3adefcfc0282c05a0e03c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3b90836b44dd67d282e24a14fb4aa0c449624b4b56cb1aaafd4b4aab9245a17a->leave($__internal_3b90836b44dd67d282e24a14fb4aa0c449624b4b56cb1aaafd4b4aab9245a17a_prof);

        
        $__internal_dcbfe2ec6537743096d8243037fc15e343d36f7bf4b3adefcfc0282c05a0e03c->leave($__internal_dcbfe2ec6537743096d8243037fc15e343d36f7bf4b3adefcfc0282c05a0e03c_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_da0f43ccf47fe6c0bdb27dde3ea684024464f7d56bf2c338785d4afb90b31c72 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_da0f43ccf47fe6c0bdb27dde3ea684024464f7d56bf2c338785d4afb90b31c72->enter($__internal_da0f43ccf47fe6c0bdb27dde3ea684024464f7d56bf2c338785d4afb90b31c72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_ff969bd686e9b203584e6ff7a05999574ed4b75c700629c83a6159d0f9e2070d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ff969bd686e9b203584e6ff7a05999574ed4b75c700629c83a6159d0f9e2070d->enter($__internal_ff969bd686e9b203584e6ff7a05999574ed4b75c700629c83a6159d0f9e2070d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_ff969bd686e9b203584e6ff7a05999574ed4b75c700629c83a6159d0f9e2070d->leave($__internal_ff969bd686e9b203584e6ff7a05999574ed4b75c700629c83a6159d0f9e2070d_prof);

        
        $__internal_da0f43ccf47fe6c0bdb27dde3ea684024464f7d56bf2c338785d4afb90b31c72->leave($__internal_da0f43ccf47fe6c0bdb27dde3ea684024464f7d56bf2c338785d4afb90b31c72_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "@FOSUser/Security/login.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login.html.twig");
    }
}
