<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_309d080f5974df84644bd4e4095d9e0c79c337c730538a651b6f3c6c15f236eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_de66e2a353cb7a0a005a239ff96b04dc7c108f8e7b3d749317ca89c3e7c8c7e9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_de66e2a353cb7a0a005a239ff96b04dc7c108f8e7b3d749317ca89c3e7c8c7e9->enter($__internal_de66e2a353cb7a0a005a239ff96b04dc7c108f8e7b3d749317ca89c3e7c8c7e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_e06245b5b038304c267fe2db96d83ad6f9cf7069f64cb43f9ae8991859ca87ce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e06245b5b038304c267fe2db96d83ad6f9cf7069f64cb43f9ae8991859ca87ce->enter($__internal_e06245b5b038304c267fe2db96d83ad6f9cf7069f64cb43f9ae8991859ca87ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_de66e2a353cb7a0a005a239ff96b04dc7c108f8e7b3d749317ca89c3e7c8c7e9->leave($__internal_de66e2a353cb7a0a005a239ff96b04dc7c108f8e7b3d749317ca89c3e7c8c7e9_prof);

        
        $__internal_e06245b5b038304c267fe2db96d83ad6f9cf7069f64cb43f9ae8991859ca87ce->leave($__internal_e06245b5b038304c267fe2db96d83ad6f9cf7069f64cb43f9ae8991859ca87ce_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}
