<?php

/* base.html.twig */
class __TwigTemplate_d81bae7d5325a7f23ba92e849144467ab8ed2421c201cbf1d70c17582ad76de7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ca25c2fc6f191bcd75ce3219e240aee6ef901041178fcc1211c2859e2319cef8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ca25c2fc6f191bcd75ce3219e240aee6ef901041178fcc1211c2859e2319cef8->enter($__internal_ca25c2fc6f191bcd75ce3219e240aee6ef901041178fcc1211c2859e2319cef8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_bf45450f28cc8357398de69ec776d95f67df9ac3cc810fc6a0e7e40b98a19c80 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf45450f28cc8357398de69ec776d95f67df9ac3cc810fc6a0e7e40b98a19c80->enter($__internal_bf45450f28cc8357398de69ec776d95f67df9ac3cc810fc6a0e7e40b98a19c80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_ca25c2fc6f191bcd75ce3219e240aee6ef901041178fcc1211c2859e2319cef8->leave($__internal_ca25c2fc6f191bcd75ce3219e240aee6ef901041178fcc1211c2859e2319cef8_prof);

        
        $__internal_bf45450f28cc8357398de69ec776d95f67df9ac3cc810fc6a0e7e40b98a19c80->leave($__internal_bf45450f28cc8357398de69ec776d95f67df9ac3cc810fc6a0e7e40b98a19c80_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_5b25215082ebb70716c40221f33d023201883e34dd0de60ad1c61f697d53a80b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b25215082ebb70716c40221f33d023201883e34dd0de60ad1c61f697d53a80b->enter($__internal_5b25215082ebb70716c40221f33d023201883e34dd0de60ad1c61f697d53a80b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_fcdfdba2dcfe1ac849d8336ffcb32d3bc979a5ee08d860ff02c20ad241991a71 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fcdfdba2dcfe1ac849d8336ffcb32d3bc979a5ee08d860ff02c20ad241991a71->enter($__internal_fcdfdba2dcfe1ac849d8336ffcb32d3bc979a5ee08d860ff02c20ad241991a71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_fcdfdba2dcfe1ac849d8336ffcb32d3bc979a5ee08d860ff02c20ad241991a71->leave($__internal_fcdfdba2dcfe1ac849d8336ffcb32d3bc979a5ee08d860ff02c20ad241991a71_prof);

        
        $__internal_5b25215082ebb70716c40221f33d023201883e34dd0de60ad1c61f697d53a80b->leave($__internal_5b25215082ebb70716c40221f33d023201883e34dd0de60ad1c61f697d53a80b_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_29d3b7d7a3e657b69b63515abe0514b99403cee01fa298b5e6c6612e8a737323 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_29d3b7d7a3e657b69b63515abe0514b99403cee01fa298b5e6c6612e8a737323->enter($__internal_29d3b7d7a3e657b69b63515abe0514b99403cee01fa298b5e6c6612e8a737323_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_d6677cc34258d1c14b70b938db35ddd622d63e6174f5bc65e2d53e1136386ead = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d6677cc34258d1c14b70b938db35ddd622d63e6174f5bc65e2d53e1136386ead->enter($__internal_d6677cc34258d1c14b70b938db35ddd622d63e6174f5bc65e2d53e1136386ead_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_d6677cc34258d1c14b70b938db35ddd622d63e6174f5bc65e2d53e1136386ead->leave($__internal_d6677cc34258d1c14b70b938db35ddd622d63e6174f5bc65e2d53e1136386ead_prof);

        
        $__internal_29d3b7d7a3e657b69b63515abe0514b99403cee01fa298b5e6c6612e8a737323->leave($__internal_29d3b7d7a3e657b69b63515abe0514b99403cee01fa298b5e6c6612e8a737323_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_a3abf66a5098cb5544674a633fb52376ca5177c2d860242a7ba64f4ddf6fa8b8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a3abf66a5098cb5544674a633fb52376ca5177c2d860242a7ba64f4ddf6fa8b8->enter($__internal_a3abf66a5098cb5544674a633fb52376ca5177c2d860242a7ba64f4ddf6fa8b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_9aae67477d9a4081230732899d3f2fdb50e2cf29147ef31167567bdc06b5e636 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9aae67477d9a4081230732899d3f2fdb50e2cf29147ef31167567bdc06b5e636->enter($__internal_9aae67477d9a4081230732899d3f2fdb50e2cf29147ef31167567bdc06b5e636_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_9aae67477d9a4081230732899d3f2fdb50e2cf29147ef31167567bdc06b5e636->leave($__internal_9aae67477d9a4081230732899d3f2fdb50e2cf29147ef31167567bdc06b5e636_prof);

        
        $__internal_a3abf66a5098cb5544674a633fb52376ca5177c2d860242a7ba64f4ddf6fa8b8->leave($__internal_a3abf66a5098cb5544674a633fb52376ca5177c2d860242a7ba64f4ddf6fa8b8_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_8709d6f8d754182b729406e290a6a999f0677f5f684da26313e5ac99050896c8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8709d6f8d754182b729406e290a6a999f0677f5f684da26313e5ac99050896c8->enter($__internal_8709d6f8d754182b729406e290a6a999f0677f5f684da26313e5ac99050896c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_9156a9b2afd4fc0c3ebb80be57eea8ee1978f450edc56197d377c32518f35c66 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9156a9b2afd4fc0c3ebb80be57eea8ee1978f450edc56197d377c32518f35c66->enter($__internal_9156a9b2afd4fc0c3ebb80be57eea8ee1978f450edc56197d377c32518f35c66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_9156a9b2afd4fc0c3ebb80be57eea8ee1978f450edc56197d377c32518f35c66->leave($__internal_9156a9b2afd4fc0c3ebb80be57eea8ee1978f450edc56197d377c32518f35c66_prof);

        
        $__internal_8709d6f8d754182b729406e290a6a999f0677f5f684da26313e5ac99050896c8->leave($__internal_8709d6f8d754182b729406e290a6a999f0677f5f684da26313e5ac99050896c8_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/app/Resources/views/base.html.twig");
    }
}
