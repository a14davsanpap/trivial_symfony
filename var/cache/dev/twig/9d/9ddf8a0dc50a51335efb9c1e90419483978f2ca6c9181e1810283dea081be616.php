<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_5c39970d0f62eebec53d6d8a047872f68deb9b53c68121b1736044beb2a3c477 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_29fbaeea8a1611d8ff38f3965476a220f58774ba47fac67d2d646c94ac816a58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_29fbaeea8a1611d8ff38f3965476a220f58774ba47fac67d2d646c94ac816a58->enter($__internal_29fbaeea8a1611d8ff38f3965476a220f58774ba47fac67d2d646c94ac816a58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_9c9c361ab2e6ce9b535992496c277223c3618c03c01290ebb561dc65b6caa7bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c9c361ab2e6ce9b535992496c277223c3618c03c01290ebb561dc65b6caa7bc->enter($__internal_9c9c361ab2e6ce9b535992496c277223c3618c03c01290ebb561dc65b6caa7bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_29fbaeea8a1611d8ff38f3965476a220f58774ba47fac67d2d646c94ac816a58->leave($__internal_29fbaeea8a1611d8ff38f3965476a220f58774ba47fac67d2d646c94ac816a58_prof);

        
        $__internal_9c9c361ab2e6ce9b535992496c277223c3618c03c01290ebb561dc65b6caa7bc->leave($__internal_9c9c361ab2e6ce9b535992496c277223c3618c03c01290ebb561dc65b6caa7bc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_enctype.html.php");
    }
}
