<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_12f738d54aff3bf3f6a56dc81f2505fdcba5d762d3d028c6a50f1a44ad74da5e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ae731191c761a106c77c82069b32b80922fa1d3ab1a5eda136e19239f8566016 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae731191c761a106c77c82069b32b80922fa1d3ab1a5eda136e19239f8566016->enter($__internal_ae731191c761a106c77c82069b32b80922fa1d3ab1a5eda136e19239f8566016_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_540c903f1ee11c42e682ab6ec53db468e39d08d580c829297a2497efd4428a57 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_540c903f1ee11c42e682ab6ec53db468e39d08d580c829297a2497efd4428a57->enter($__internal_540c903f1ee11c42e682ab6ec53db468e39d08d580c829297a2497efd4428a57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ae731191c761a106c77c82069b32b80922fa1d3ab1a5eda136e19239f8566016->leave($__internal_ae731191c761a106c77c82069b32b80922fa1d3ab1a5eda136e19239f8566016_prof);

        
        $__internal_540c903f1ee11c42e682ab6ec53db468e39d08d580c829297a2497efd4428a57->leave($__internal_540c903f1ee11c42e682ab6ec53db468e39d08d580c829297a2497efd4428a57_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_6acdf7b17450551ce807f8713b6920d07cc7141a21e00d78ca30ccc158b4d90d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6acdf7b17450551ce807f8713b6920d07cc7141a21e00d78ca30ccc158b4d90d->enter($__internal_6acdf7b17450551ce807f8713b6920d07cc7141a21e00d78ca30ccc158b4d90d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_84d316f80de896100ce2a7dc75d93f9185ce901a3d430a5715f7edb06db31b3d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_84d316f80de896100ce2a7dc75d93f9185ce901a3d430a5715f7edb06db31b3d->enter($__internal_84d316f80de896100ce2a7dc75d93f9185ce901a3d430a5715f7edb06db31b3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_84d316f80de896100ce2a7dc75d93f9185ce901a3d430a5715f7edb06db31b3d->leave($__internal_84d316f80de896100ce2a7dc75d93f9185ce901a3d430a5715f7edb06db31b3d_prof);

        
        $__internal_6acdf7b17450551ce807f8713b6920d07cc7141a21e00d78ca30ccc158b4d90d->leave($__internal_6acdf7b17450551ce807f8713b6920d07cc7141a21e00d78ca30ccc158b4d90d_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_0bd848a31e193985429a5a4a293acfa8e2bf8be51601a82e44f2645635dcd021 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0bd848a31e193985429a5a4a293acfa8e2bf8be51601a82e44f2645635dcd021->enter($__internal_0bd848a31e193985429a5a4a293acfa8e2bf8be51601a82e44f2645635dcd021_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_6307e4c9ae6b4e4a2636bd2a532ec403b131fc759ad44f939700fc0ca8f01808 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6307e4c9ae6b4e4a2636bd2a532ec403b131fc759ad44f939700fc0ca8f01808->enter($__internal_6307e4c9ae6b4e4a2636bd2a532ec403b131fc759ad44f939700fc0ca8f01808_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_6307e4c9ae6b4e4a2636bd2a532ec403b131fc759ad44f939700fc0ca8f01808->leave($__internal_6307e4c9ae6b4e4a2636bd2a532ec403b131fc759ad44f939700fc0ca8f01808_prof);

        
        $__internal_0bd848a31e193985429a5a4a293acfa8e2bf8be51601a82e44f2645635dcd021->leave($__internal_0bd848a31e193985429a5a4a293acfa8e2bf8be51601a82e44f2645635dcd021_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_39e3ffe6a6e7d58d2fcd6a8c43c77af7c7f10b1fc7e25d05bc746abd82072e8e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_39e3ffe6a6e7d58d2fcd6a8c43c77af7c7f10b1fc7e25d05bc746abd82072e8e->enter($__internal_39e3ffe6a6e7d58d2fcd6a8c43c77af7c7f10b1fc7e25d05bc746abd82072e8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_9cd6518e703a4166b95c96392bc2f5dd3fb5641cc7fb6647c503a15cacea3877 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9cd6518e703a4166b95c96392bc2f5dd3fb5641cc7fb6647c503a15cacea3877->enter($__internal_9cd6518e703a4166b95c96392bc2f5dd3fb5641cc7fb6647c503a15cacea3877_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_9cd6518e703a4166b95c96392bc2f5dd3fb5641cc7fb6647c503a15cacea3877->leave($__internal_9cd6518e703a4166b95c96392bc2f5dd3fb5641cc7fb6647c503a15cacea3877_prof);

        
        $__internal_39e3ffe6a6e7d58d2fcd6a8c43c77af7c7f10b1fc7e25d05bc746abd82072e8e->leave($__internal_39e3ffe6a6e7d58d2fcd6a8c43c77af7c7f10b1fc7e25d05bc746abd82072e8e_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
