<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_3bad7450074ba7743855011c96717adf4a3b177996b5c74cc2aa439f6c0a2c28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_12a395c901f152e7379722834cbbf7a622ca2e358d035ea096c6605a09eded4e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_12a395c901f152e7379722834cbbf7a622ca2e358d035ea096c6605a09eded4e->enter($__internal_12a395c901f152e7379722834cbbf7a622ca2e358d035ea096c6605a09eded4e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        $__internal_d7571c9487a316d3d55bf14cb0852075468ac77247d0887e359b436b5662a879 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7571c9487a316d3d55bf14cb0852075468ac77247d0887e359b436b5662a879->enter($__internal_d7571c9487a316d3d55bf14cb0852075468ac77247d0887e359b436b5662a879_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_12a395c901f152e7379722834cbbf7a622ca2e358d035ea096c6605a09eded4e->leave($__internal_12a395c901f152e7379722834cbbf7a622ca2e358d035ea096c6605a09eded4e_prof);

        
        $__internal_d7571c9487a316d3d55bf14cb0852075468ac77247d0887e359b436b5662a879->leave($__internal_d7571c9487a316d3d55bf14cb0852075468ac77247d0887e359b436b5662a879_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_rows') ?>
", "@Framework/Form/repeated_row.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/repeated_row.html.php");
    }
}
