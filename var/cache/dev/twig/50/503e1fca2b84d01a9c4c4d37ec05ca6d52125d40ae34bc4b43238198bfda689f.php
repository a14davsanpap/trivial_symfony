<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_d7dbf3db3bb7bed2a3f4feef7501c2394c961991fa14539e0d0c8dba0b6de77c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bef579e730d0d13bb3ff0618dda4e3d43414d13d510c7f459a5e39173f7629ed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bef579e730d0d13bb3ff0618dda4e3d43414d13d510c7f459a5e39173f7629ed->enter($__internal_bef579e730d0d13bb3ff0618dda4e3d43414d13d510c7f459a5e39173f7629ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_a22d1491daef26daeba6eb85597658722cf1b3fb8798df0f6a6bed8a10b5c827 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a22d1491daef26daeba6eb85597658722cf1b3fb8798df0f6a6bed8a10b5c827->enter($__internal_a22d1491daef26daeba6eb85597658722cf1b3fb8798df0f6a6bed8a10b5c827_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_bef579e730d0d13bb3ff0618dda4e3d43414d13d510c7f459a5e39173f7629ed->leave($__internal_bef579e730d0d13bb3ff0618dda4e3d43414d13d510c7f459a5e39173f7629ed_prof);

        
        $__internal_a22d1491daef26daeba6eb85597658722cf1b3fb8798df0f6a6bed8a10b5c827->leave($__internal_a22d1491daef26daeba6eb85597658722cf1b3fb8798df0f6a6bed8a10b5c827_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}
