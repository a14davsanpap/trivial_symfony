<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_29a02bedfe3cf26583b09ebb4c87ac7db56658eaf9c23ac541dea77d8510f7f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_99d7cb0bd45697b57d599f5c5e28f1104a59341c0e476f6b89860702a1313b15 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_99d7cb0bd45697b57d599f5c5e28f1104a59341c0e476f6b89860702a1313b15->enter($__internal_99d7cb0bd45697b57d599f5c5e28f1104a59341c0e476f6b89860702a1313b15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        $__internal_1210ce76e607f3025a63d10c3887bb1fb7c868b4762297ac665394f970465053 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1210ce76e607f3025a63d10c3887bb1fb7c868b4762297ac665394f970465053->enter($__internal_1210ce76e607f3025a63d10c3887bb1fb7c868b4762297ac665394f970465053_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_99d7cb0bd45697b57d599f5c5e28f1104a59341c0e476f6b89860702a1313b15->leave($__internal_99d7cb0bd45697b57d599f5c5e28f1104a59341c0e476f6b89860702a1313b15_prof);

        
        $__internal_1210ce76e607f3025a63d10c3887bb1fb7c868b4762297ac665394f970465053->leave($__internal_1210ce76e607f3025a63d10c3887bb1fb7c868b4762297ac665394f970465053_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_afb594e0f456424b3a9b1c23aa84f9479712bac376592b63d99c76906e9af43c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_afb594e0f456424b3a9b1c23aa84f9479712bac376592b63d99c76906e9af43c->enter($__internal_afb594e0f456424b3a9b1c23aa84f9479712bac376592b63d99c76906e9af43c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_7b5872ed06537c7116429b9242eb63fd7b834901290c02f86f0806d26b2eeff4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7b5872ed06537c7116429b9242eb63fd7b834901290c02f86f0806d26b2eeff4->enter($__internal_7b5872ed06537c7116429b9242eb63fd7b834901290c02f86f0806d26b2eeff4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        
        $__internal_7b5872ed06537c7116429b9242eb63fd7b834901290c02f86f0806d26b2eeff4->leave($__internal_7b5872ed06537c7116429b9242eb63fd7b834901290c02f86f0806d26b2eeff4_prof);

        
        $__internal_afb594e0f456424b3a9b1c23aa84f9479712bac376592b63d99c76906e9af43c->leave($__internal_afb594e0f456424b3a9b1c23aa84f9479712bac376592b63d99c76906e9af43c_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_1323055d1d5eee26c2cfd11278ee6e4697cf0c8295191f4d20e406e597deae43 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1323055d1d5eee26c2cfd11278ee6e4697cf0c8295191f4d20e406e597deae43->enter($__internal_1323055d1d5eee26c2cfd11278ee6e4697cf0c8295191f4d20e406e597deae43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_f973b862ed49078a52c3e1d02654069f18b4a2713d3e4172dd95f073bdad3d87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f973b862ed49078a52c3e1d02654069f18b4a2713d3e4172dd95f073bdad3d87->enter($__internal_f973b862ed49078a52c3e1d02654069f18b4a2713d3e4172dd95f073bdad3d87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_f973b862ed49078a52c3e1d02654069f18b4a2713d3e4172dd95f073bdad3d87->leave($__internal_f973b862ed49078a52c3e1d02654069f18b4a2713d3e4172dd95f073bdad3d87_prof);

        
        $__internal_1323055d1d5eee26c2cfd11278ee6e4697cf0c8295191f4d20e406e597deae43->leave($__internal_1323055d1d5eee26c2cfd11278ee6e4697cf0c8295191f4d20e406e597deae43_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_89cd40065b88520f0ff6378cca2c0d0caffe8b1785989dda3319ef1b8d0d1405 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_89cd40065b88520f0ff6378cca2c0d0caffe8b1785989dda3319ef1b8d0d1405->enter($__internal_89cd40065b88520f0ff6378cca2c0d0caffe8b1785989dda3319ef1b8d0d1405_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_fec2980aef60aacc21ba35e223799a8d6b5289aeff9e0c306655d29cf59bd3c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fec2980aef60aacc21ba35e223799a8d6b5289aeff9e0c306655d29cf59bd3c5->enter($__internal_fec2980aef60aacc21ba35e223799a8d6b5289aeff9e0c306655d29cf59bd3c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_fec2980aef60aacc21ba35e223799a8d6b5289aeff9e0c306655d29cf59bd3c5->leave($__internal_fec2980aef60aacc21ba35e223799a8d6b5289aeff9e0c306655d29cf59bd3c5_prof);

        
        $__internal_89cd40065b88520f0ff6378cca2c0d0caffe8b1785989dda3319ef1b8d0d1405->leave($__internal_89cd40065b88520f0ff6378cca2c0d0caffe8b1785989dda3319ef1b8d0d1405_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Registration:email.txt.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/email.txt.twig");
    }
}
