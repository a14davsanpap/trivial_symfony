<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_a6f555ef5decde6e746e50e97fdf645ccf24d9caaef91a3183da4cf2bf7c06b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_41c521f594f8d42b3983e0c46d4583f97edff0ce3b883ab251cc4595847a2c98 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_41c521f594f8d42b3983e0c46d4583f97edff0ce3b883ab251cc4595847a2c98->enter($__internal_41c521f594f8d42b3983e0c46d4583f97edff0ce3b883ab251cc4595847a2c98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $__internal_c9acdc5fdfe5b90a365b207ba944abf8bb7dd0db035e20f2621b28b88ca49eb5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c9acdc5fdfe5b90a365b207ba944abf8bb7dd0db035e20f2621b28b88ca49eb5->enter($__internal_c9acdc5fdfe5b90a365b207ba944abf8bb7dd0db035e20f2621b28b88ca49eb5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_41c521f594f8d42b3983e0c46d4583f97edff0ce3b883ab251cc4595847a2c98->leave($__internal_41c521f594f8d42b3983e0c46d4583f97edff0ce3b883ab251cc4595847a2c98_prof);

        
        $__internal_c9acdc5fdfe5b90a365b207ba944abf8bb7dd0db035e20f2621b28b88ca49eb5->leave($__internal_c9acdc5fdfe5b90a365b207ba944abf8bb7dd0db035e20f2621b28b88ca49eb5_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_99fd2c596cb265ea47f44b0b01ae1cd0147064e1dc318adcc247f372eb710653 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_99fd2c596cb265ea47f44b0b01ae1cd0147064e1dc318adcc247f372eb710653->enter($__internal_99fd2c596cb265ea47f44b0b01ae1cd0147064e1dc318adcc247f372eb710653_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_36902fc05ebe4f7ef9cf6e06a6ae4d4e6eabe1d524016219487130168a400745 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36902fc05ebe4f7ef9cf6e06a6ae4d4e6eabe1d524016219487130168a400745->enter($__internal_36902fc05ebe4f7ef9cf6e06a6ae4d4e6eabe1d524016219487130168a400745_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_36902fc05ebe4f7ef9cf6e06a6ae4d4e6eabe1d524016219487130168a400745->leave($__internal_36902fc05ebe4f7ef9cf6e06a6ae4d4e6eabe1d524016219487130168a400745_prof);

        
        $__internal_99fd2c596cb265ea47f44b0b01ae1cd0147064e1dc318adcc247f372eb710653->leave($__internal_99fd2c596cb265ea47f44b0b01ae1cd0147064e1dc318adcc247f372eb710653_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:edit.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/friendsofsymfony/user-bundle/Resources/views/Group/edit.html.twig");
    }
}
