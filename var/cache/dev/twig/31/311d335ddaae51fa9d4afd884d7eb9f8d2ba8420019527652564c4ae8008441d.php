<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_67b6c4b75c73ea256d9274ba7abce871d65d9a6f616988fe80da1cc8701da5cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3ffea829b72729fcaf3b91dbf35e3111d9477c98d24f613a2a8484e01ba22b70 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3ffea829b72729fcaf3b91dbf35e3111d9477c98d24f613a2a8484e01ba22b70->enter($__internal_3ffea829b72729fcaf3b91dbf35e3111d9477c98d24f613a2a8484e01ba22b70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_c30b2fcf81d57f4d9c6997efa52caf7528b31ab72606e42278d90d2066cd273e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c30b2fcf81d57f4d9c6997efa52caf7528b31ab72606e42278d90d2066cd273e->enter($__internal_c30b2fcf81d57f4d9c6997efa52caf7528b31ab72606e42278d90d2066cd273e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_3ffea829b72729fcaf3b91dbf35e3111d9477c98d24f613a2a8484e01ba22b70->leave($__internal_3ffea829b72729fcaf3b91dbf35e3111d9477c98d24f613a2a8484e01ba22b70_prof);

        
        $__internal_c30b2fcf81d57f4d9c6997efa52caf7528b31ab72606e42278d90d2066cd273e->leave($__internal_c30b2fcf81d57f4d9c6997efa52caf7528b31ab72606e42278d90d2066cd273e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}
