<?php

/* @FOSUser/Registration/register.html.twig */
class __TwigTemplate_3bbb6c6803d3f6a382be84877d6b3883a0ab65186a35fac3f7b35f8c4c47b064 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3594548ef7f105edf94dc8490f99841b2ec4bddad160e6f218934ba2d42467ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3594548ef7f105edf94dc8490f99841b2ec4bddad160e6f218934ba2d42467ba->enter($__internal_3594548ef7f105edf94dc8490f99841b2ec4bddad160e6f218934ba2d42467ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $__internal_c1f697a768d1d6d916b08629417ffc9ecab750ada9763a64e5fdb63bf7311af1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1f697a768d1d6d916b08629417ffc9ecab750ada9763a64e5fdb63bf7311af1->enter($__internal_c1f697a768d1d6d916b08629417ffc9ecab750ada9763a64e5fdb63bf7311af1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3594548ef7f105edf94dc8490f99841b2ec4bddad160e6f218934ba2d42467ba->leave($__internal_3594548ef7f105edf94dc8490f99841b2ec4bddad160e6f218934ba2d42467ba_prof);

        
        $__internal_c1f697a768d1d6d916b08629417ffc9ecab750ada9763a64e5fdb63bf7311af1->leave($__internal_c1f697a768d1d6d916b08629417ffc9ecab750ada9763a64e5fdb63bf7311af1_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_86ee1d1bd72695f0826e216c77700a0add0aa8a4df88ae21022256d9f480bac1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_86ee1d1bd72695f0826e216c77700a0add0aa8a4df88ae21022256d9f480bac1->enter($__internal_86ee1d1bd72695f0826e216c77700a0add0aa8a4df88ae21022256d9f480bac1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_521f2e73fab868b4e475be5004810dd6ce66e848938c693721e1bd5993f67ab3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_521f2e73fab868b4e475be5004810dd6ce66e848938c693721e1bd5993f67ab3->enter($__internal_521f2e73fab868b4e475be5004810dd6ce66e848938c693721e1bd5993f67ab3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "@FOSUser/Registration/register.html.twig", 4)->display($context);
        
        $__internal_521f2e73fab868b4e475be5004810dd6ce66e848938c693721e1bd5993f67ab3->leave($__internal_521f2e73fab868b4e475be5004810dd6ce66e848938c693721e1bd5993f67ab3_prof);

        
        $__internal_86ee1d1bd72695f0826e216c77700a0add0aa8a4df88ae21022256d9f480bac1->leave($__internal_86ee1d1bd72695f0826e216c77700a0add0aa8a4df88ae21022256d9f480bac1_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Registration/register.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register.html.twig");
    }
}
