<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_95d7afa5dccedf33d5c5855dd7cff6cece12ad2c9179c95e5f380d3d7bc24e66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9c42ba563ddd10654d377c49e212efb8e4ec2a8aaf3d6bd136093ea99eb92a50 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9c42ba563ddd10654d377c49e212efb8e4ec2a8aaf3d6bd136093ea99eb92a50->enter($__internal_9c42ba563ddd10654d377c49e212efb8e4ec2a8aaf3d6bd136093ea99eb92a50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_74c89ed91f495f8ac14ec25b05fb5b436d2c0bb55995cea02bae9ca83593686d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_74c89ed91f495f8ac14ec25b05fb5b436d2c0bb55995cea02bae9ca83593686d->enter($__internal_74c89ed91f495f8ac14ec25b05fb5b436d2c0bb55995cea02bae9ca83593686d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_9c42ba563ddd10654d377c49e212efb8e4ec2a8aaf3d6bd136093ea99eb92a50->leave($__internal_9c42ba563ddd10654d377c49e212efb8e4ec2a8aaf3d6bd136093ea99eb92a50_prof);

        
        $__internal_74c89ed91f495f8ac14ec25b05fb5b436d2c0bb55995cea02bae9ca83593686d->leave($__internal_74c89ed91f495f8ac14ec25b05fb5b436d2c0bb55995cea02bae9ca83593686d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/integer_widget.html.php");
    }
}
