<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_c68e8f3556d491c6a7920c412510dd2829b44d6b0957808f7a1412d0e2a0a883 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_601a8cba6ac3ec20686b9ae053b6b6af70a04ad17b55d9bb93cf199fe1e9645c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_601a8cba6ac3ec20686b9ae053b6b6af70a04ad17b55d9bb93cf199fe1e9645c->enter($__internal_601a8cba6ac3ec20686b9ae053b6b6af70a04ad17b55d9bb93cf199fe1e9645c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        $__internal_34c389d7f84e2315305db4c9defb5d2fbdce67d05e353c880ba9f9b41e961ee4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_34c389d7f84e2315305db4c9defb5d2fbdce67d05e353c880ba9f9b41e961ee4->enter($__internal_34c389d7f84e2315305db4c9defb5d2fbdce67d05e353c880ba9f9b41e961ee4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_601a8cba6ac3ec20686b9ae053b6b6af70a04ad17b55d9bb93cf199fe1e9645c->leave($__internal_601a8cba6ac3ec20686b9ae053b6b6af70a04ad17b55d9bb93cf199fe1e9645c_prof);

        
        $__internal_34c389d7f84e2315305db4c9defb5d2fbdce67d05e353c880ba9f9b41e961ee4->leave($__internal_34c389d7f84e2315305db4c9defb5d2fbdce67d05e353c880ba9f9b41e961ee4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/button_row.html.php");
    }
}
