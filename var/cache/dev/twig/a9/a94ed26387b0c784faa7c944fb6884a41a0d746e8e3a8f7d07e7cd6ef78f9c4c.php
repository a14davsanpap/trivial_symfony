<?php

/* FOSUserBundle:Registration:check_email.html.twig */
class __TwigTemplate_ad792c91016a914b9e9cf8dcf0fdcf2a7d265db7e0b42382f80d67c5b4e249e5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_28603cfbcd197a0c0aab135ba9c25b5adc996d1ee9f4fa8cfab74286c457974e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_28603cfbcd197a0c0aab135ba9c25b5adc996d1ee9f4fa8cfab74286c457974e->enter($__internal_28603cfbcd197a0c0aab135ba9c25b5adc996d1ee9f4fa8cfab74286c457974e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $__internal_5d9c0ca05906dc03463a4450e2432e31014a297514b045b5e7be81773a24cd4a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5d9c0ca05906dc03463a4450e2432e31014a297514b045b5e7be81773a24cd4a->enter($__internal_5d9c0ca05906dc03463a4450e2432e31014a297514b045b5e7be81773a24cd4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_28603cfbcd197a0c0aab135ba9c25b5adc996d1ee9f4fa8cfab74286c457974e->leave($__internal_28603cfbcd197a0c0aab135ba9c25b5adc996d1ee9f4fa8cfab74286c457974e_prof);

        
        $__internal_5d9c0ca05906dc03463a4450e2432e31014a297514b045b5e7be81773a24cd4a->leave($__internal_5d9c0ca05906dc03463a4450e2432e31014a297514b045b5e7be81773a24cd4a_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_375a1897fb6c87e9603fbf3565e0d59d882071264ec15b75871fb0fd61f3aeff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_375a1897fb6c87e9603fbf3565e0d59d882071264ec15b75871fb0fd61f3aeff->enter($__internal_375a1897fb6c87e9603fbf3565e0d59d882071264ec15b75871fb0fd61f3aeff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_3cfcc4bdaa05e007261ccdfc3a12a48e87838b10dbd2f40c19c22d5fde11fcd6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3cfcc4bdaa05e007261ccdfc3a12a48e87838b10dbd2f40c19c22d5fde11fcd6->enter($__internal_3cfcc4bdaa05e007261ccdfc3a12a48e87838b10dbd2f40c19c22d5fde11fcd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_3cfcc4bdaa05e007261ccdfc3a12a48e87838b10dbd2f40c19c22d5fde11fcd6->leave($__internal_3cfcc4bdaa05e007261ccdfc3a12a48e87838b10dbd2f40c19c22d5fde11fcd6_prof);

        
        $__internal_375a1897fb6c87e9603fbf3565e0d59d882071264ec15b75871fb0fd61f3aeff->leave($__internal_375a1897fb6c87e9603fbf3565e0d59d882071264ec15b75871fb0fd61f3aeff_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:check_email.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/check_email.html.twig");
    }
}
