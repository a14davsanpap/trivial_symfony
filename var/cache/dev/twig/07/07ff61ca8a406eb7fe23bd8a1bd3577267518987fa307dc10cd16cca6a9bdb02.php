<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_5b92c25c5c9b2f32d8bbc27cab807545e111bb03e7b13183f129d14b34485e3a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1189388a1c2dc81d75b269703837a8ee07adeee6c7e8b65b7bda51b87ea25878 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1189388a1c2dc81d75b269703837a8ee07adeee6c7e8b65b7bda51b87ea25878->enter($__internal_1189388a1c2dc81d75b269703837a8ee07adeee6c7e8b65b7bda51b87ea25878_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_09b3d332c1f9002c57fa828ccacc25f86d1b629a822985dc1d541d5069434c84 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09b3d332c1f9002c57fa828ccacc25f86d1b629a822985dc1d541d5069434c84->enter($__internal_09b3d332c1f9002c57fa828ccacc25f86d1b629a822985dc1d541d5069434c84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_1189388a1c2dc81d75b269703837a8ee07adeee6c7e8b65b7bda51b87ea25878->leave($__internal_1189388a1c2dc81d75b269703837a8ee07adeee6c7e8b65b7bda51b87ea25878_prof);

        
        $__internal_09b3d332c1f9002c57fa828ccacc25f86d1b629a822985dc1d541d5069434c84->leave($__internal_09b3d332c1f9002c57fa828ccacc25f86d1b629a822985dc1d541d5069434c84_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rest.html.php");
    }
}
