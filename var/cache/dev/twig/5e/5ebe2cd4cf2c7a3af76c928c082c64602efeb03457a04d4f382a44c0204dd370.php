<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_fc798587f5db9024bc481f64274ea504183d89f62fe124609595832b72c8d6c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_540f495a0fcaf389bce1625473a0737753b4d151b1b70061a9dfc165f618adae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_540f495a0fcaf389bce1625473a0737753b4d151b1b70061a9dfc165f618adae->enter($__internal_540f495a0fcaf389bce1625473a0737753b4d151b1b70061a9dfc165f618adae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $__internal_06313465b167ad0312a9a985afff5013d57a526d0e51d84fb760ec8ffa6c74cc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_06313465b167ad0312a9a985afff5013d57a526d0e51d84fb760ec8ffa6c74cc->enter($__internal_06313465b167ad0312a9a985afff5013d57a526d0e51d84fb760ec8ffa6c74cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_540f495a0fcaf389bce1625473a0737753b4d151b1b70061a9dfc165f618adae->leave($__internal_540f495a0fcaf389bce1625473a0737753b4d151b1b70061a9dfc165f618adae_prof);

        
        $__internal_06313465b167ad0312a9a985afff5013d57a526d0e51d84fb760ec8ffa6c74cc->leave($__internal_06313465b167ad0312a9a985afff5013d57a526d0e51d84fb760ec8ffa6c74cc_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_1ba184e17f5907a42268d29004b480ccf6b9b5a412b09980c1d026a7cc8a81ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1ba184e17f5907a42268d29004b480ccf6b9b5a412b09980c1d026a7cc8a81ee->enter($__internal_1ba184e17f5907a42268d29004b480ccf6b9b5a412b09980c1d026a7cc8a81ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_f7c8fcdbeb1eebe06052165c747421871563764b417356b64bfaf9bcc033c24e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f7c8fcdbeb1eebe06052165c747421871563764b417356b64bfaf9bcc033c24e->enter($__internal_f7c8fcdbeb1eebe06052165c747421871563764b417356b64bfaf9bcc033c24e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if (($context["targetUrl"] ?? $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, ($context["targetUrl"] ?? $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_f7c8fcdbeb1eebe06052165c747421871563764b417356b64bfaf9bcc033c24e->leave($__internal_f7c8fcdbeb1eebe06052165c747421871563764b417356b64bfaf9bcc033c24e_prof);

        
        $__internal_1ba184e17f5907a42268d29004b480ccf6b9b5a412b09980c1d026a7cc8a81ee->leave($__internal_1ba184e17f5907a42268d29004b480ccf6b9b5a412b09980c1d026a7cc8a81ee_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:confirmed.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/confirmed.html.twig");
    }
}
