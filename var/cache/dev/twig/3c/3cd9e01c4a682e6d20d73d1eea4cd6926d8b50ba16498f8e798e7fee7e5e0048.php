<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_490088ef8c2d536ba9aa3e4da532e3c9d5d6875dde7bb492995d7d77f4e311f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8cc8125910add2c34356189761f15811310a14246a35f9f9edf7114515149a89 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8cc8125910add2c34356189761f15811310a14246a35f9f9edf7114515149a89->enter($__internal_8cc8125910add2c34356189761f15811310a14246a35f9f9edf7114515149a89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_4c1f38c8462e6f1d8d6ecb24095ef05d38e3d572ce38485c891c7d06e313901d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c1f38c8462e6f1d8d6ecb24095ef05d38e3d572ce38485c891c7d06e313901d->enter($__internal_4c1f38c8462e6f1d8d6ecb24095ef05d38e3d572ce38485c891c7d06e313901d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8cc8125910add2c34356189761f15811310a14246a35f9f9edf7114515149a89->leave($__internal_8cc8125910add2c34356189761f15811310a14246a35f9f9edf7114515149a89_prof);

        
        $__internal_4c1f38c8462e6f1d8d6ecb24095ef05d38e3d572ce38485c891c7d06e313901d->leave($__internal_4c1f38c8462e6f1d8d6ecb24095ef05d38e3d572ce38485c891c7d06e313901d_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_01b691c89d51a3ba37cb59f0514c23fba1b871b01f4bebbc1dbdeda2d0a43e6d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01b691c89d51a3ba37cb59f0514c23fba1b871b01f4bebbc1dbdeda2d0a43e6d->enter($__internal_01b691c89d51a3ba37cb59f0514c23fba1b871b01f4bebbc1dbdeda2d0a43e6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_2944eac4bee9aeefa0f4fea7dc433aab24db4855cd9aa37a4ba6647b829874af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2944eac4bee9aeefa0f4fea7dc433aab24db4855cd9aa37a4ba6647b829874af->enter($__internal_2944eac4bee9aeefa0f4fea7dc433aab24db4855cd9aa37a4ba6647b829874af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_2944eac4bee9aeefa0f4fea7dc433aab24db4855cd9aa37a4ba6647b829874af->leave($__internal_2944eac4bee9aeefa0f4fea7dc433aab24db4855cd9aa37a4ba6647b829874af_prof);

        
        $__internal_01b691c89d51a3ba37cb59f0514c23fba1b871b01f4bebbc1dbdeda2d0a43e6d->leave($__internal_01b691c89d51a3ba37cb59f0514c23fba1b871b01f4bebbc1dbdeda2d0a43e6d_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_0eb9ed092e7eb435d3b175d88b629391af841c12e53203aa0325c2ab87e20a74 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0eb9ed092e7eb435d3b175d88b629391af841c12e53203aa0325c2ab87e20a74->enter($__internal_0eb9ed092e7eb435d3b175d88b629391af841c12e53203aa0325c2ab87e20a74_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_2a9a7327f26c8c602a056b9ef83eb0abcf1d81a26223a915f9f45fddb619ef05 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a9a7327f26c8c602a056b9ef83eb0abcf1d81a26223a915f9f45fddb619ef05->enter($__internal_2a9a7327f26c8c602a056b9ef83eb0abcf1d81a26223a915f9f45fddb619ef05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_2a9a7327f26c8c602a056b9ef83eb0abcf1d81a26223a915f9f45fddb619ef05->leave($__internal_2a9a7327f26c8c602a056b9ef83eb0abcf1d81a26223a915f9f45fddb619ef05_prof);

        
        $__internal_0eb9ed092e7eb435d3b175d88b629391af841c12e53203aa0325c2ab87e20a74->leave($__internal_0eb9ed092e7eb435d3b175d88b629391af841c12e53203aa0325c2ab87e20a74_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_e813aa52b79be294f4541c3e7590a5b13be86392fb750022c45f66be24df675e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e813aa52b79be294f4541c3e7590a5b13be86392fb750022c45f66be24df675e->enter($__internal_e813aa52b79be294f4541c3e7590a5b13be86392fb750022c45f66be24df675e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_7e5ae6dba9e791b1b530a0b93c0399a2fe0cb0ddff01d5baa3af2f28357c0749 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e5ae6dba9e791b1b530a0b93c0399a2fe0cb0ddff01d5baa3af2f28357c0749->enter($__internal_7e5ae6dba9e791b1b530a0b93c0399a2fe0cb0ddff01d5baa3af2f28357c0749_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_7e5ae6dba9e791b1b530a0b93c0399a2fe0cb0ddff01d5baa3af2f28357c0749->leave($__internal_7e5ae6dba9e791b1b530a0b93c0399a2fe0cb0ddff01d5baa3af2f28357c0749_prof);

        
        $__internal_e813aa52b79be294f4541c3e7590a5b13be86392fb750022c45f66be24df675e->leave($__internal_e813aa52b79be294f4541c3e7590a5b13be86392fb750022c45f66be24df675e_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
