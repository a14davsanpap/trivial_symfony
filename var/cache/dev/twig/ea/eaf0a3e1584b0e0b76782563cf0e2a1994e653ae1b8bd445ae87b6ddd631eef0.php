<?php

/* TwigBundle:Exception:error.rdf.twig */
class __TwigTemplate_cf98befa34d6fd83c86761236c260b9a9d3e7c2c68dd45d3cce13b4178408a30 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_981fc8cff88a470c5cafe8f3f8f77dcddc47eedf445e6bf9e1a6965d339cf358 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_981fc8cff88a470c5cafe8f3f8f77dcddc47eedf445e6bf9e1a6965d339cf358->enter($__internal_981fc8cff88a470c5cafe8f3f8f77dcddc47eedf445e6bf9e1a6965d339cf358_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        $__internal_4c7592777df30ceaa1a93f3e51aa937208566568862b2071da32d15103c72268 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c7592777df30ceaa1a93f3e51aa937208566568862b2071da32d15103c72268->enter($__internal_4c7592777df30ceaa1a93f3e51aa937208566568862b2071da32d15103c72268_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.rdf.twig", 1)->display($context);
        
        $__internal_981fc8cff88a470c5cafe8f3f8f77dcddc47eedf445e6bf9e1a6965d339cf358->leave($__internal_981fc8cff88a470c5cafe8f3f8f77dcddc47eedf445e6bf9e1a6965d339cf358_prof);

        
        $__internal_4c7592777df30ceaa1a93f3e51aa937208566568862b2071da32d15103c72268->leave($__internal_4c7592777df30ceaa1a93f3e51aa937208566568862b2071da32d15103c72268_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.rdf.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/error.xml.twig' %}
", "TwigBundle:Exception:error.rdf.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.rdf.twig");
    }
}
