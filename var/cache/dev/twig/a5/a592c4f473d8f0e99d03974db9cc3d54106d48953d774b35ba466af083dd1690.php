<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_0bf77a47730e02fcf23c94ec7e89c784ea00fe6ec14f2c3a6da988c62afea9a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7bd53a9ab4e55c3152ae665d68f765691b460ae33a69c4bd7701e602fe298ebe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7bd53a9ab4e55c3152ae665d68f765691b460ae33a69c4bd7701e602fe298ebe->enter($__internal_7bd53a9ab4e55c3152ae665d68f765691b460ae33a69c4bd7701e602fe298ebe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_1259211dcbe3c4a54b9336ab5ecf305042b6de6e12153dd07b3cf212dfc2bd83 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1259211dcbe3c4a54b9336ab5ecf305042b6de6e12153dd07b3cf212dfc2bd83->enter($__internal_1259211dcbe3c4a54b9336ab5ecf305042b6de6e12153dd07b3cf212dfc2bd83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_7bd53a9ab4e55c3152ae665d68f765691b460ae33a69c4bd7701e602fe298ebe->leave($__internal_7bd53a9ab4e55c3152ae665d68f765691b460ae33a69c4bd7701e602fe298ebe_prof);

        
        $__internal_1259211dcbe3c4a54b9336ab5ecf305042b6de6e12153dd07b3cf212dfc2bd83->leave($__internal_1259211dcbe3c4a54b9336ab5ecf305042b6de6e12153dd07b3cf212dfc2bd83_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}
