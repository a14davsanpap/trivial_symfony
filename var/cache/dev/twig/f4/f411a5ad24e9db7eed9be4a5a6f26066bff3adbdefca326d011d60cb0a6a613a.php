<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_0798e37c38d513bea7b709472f00287adf735e9290f5e482edd74f4ecf2ed013 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d010081e97429d6506a09075aaa58b1ad7d4de0b73ff41fdf21bfdc411eca3ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d010081e97429d6506a09075aaa58b1ad7d4de0b73ff41fdf21bfdc411eca3ef->enter($__internal_d010081e97429d6506a09075aaa58b1ad7d4de0b73ff41fdf21bfdc411eca3ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_a8e4ea26ac1cedf41c0f9b408bf2bc86f92d658cfe86fd3a548f85232485253e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a8e4ea26ac1cedf41c0f9b408bf2bc86f92d658cfe86fd3a548f85232485253e->enter($__internal_a8e4ea26ac1cedf41c0f9b408bf2bc86f92d658cfe86fd3a548f85232485253e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_d010081e97429d6506a09075aaa58b1ad7d4de0b73ff41fdf21bfdc411eca3ef->leave($__internal_d010081e97429d6506a09075aaa58b1ad7d4de0b73ff41fdf21bfdc411eca3ef_prof);

        
        $__internal_a8e4ea26ac1cedf41c0f9b408bf2bc86f92d658cfe86fd3a548f85232485253e->leave($__internal_a8e4ea26ac1cedf41c0f9b408bf2bc86f92d658cfe86fd3a548f85232485253e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/url_widget.html.php");
    }
}
