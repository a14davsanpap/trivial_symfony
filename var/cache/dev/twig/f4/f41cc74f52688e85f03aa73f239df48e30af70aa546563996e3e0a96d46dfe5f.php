<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_f5a4e4e47a61dbc596b8a5cf6c4251cf9809cbaa7fe0192c935bf3aa4bd7f2e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1a5ed6016c0b7bd8d1054e695b36a2503992a6ff81f0897a89a5436aaa561825 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1a5ed6016c0b7bd8d1054e695b36a2503992a6ff81f0897a89a5436aaa561825->enter($__internal_1a5ed6016c0b7bd8d1054e695b36a2503992a6ff81f0897a89a5436aaa561825_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_a655d215d0b57cdfd8a0ce3dc578ef777d8343b474386d675401be18508a50fc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a655d215d0b57cdfd8a0ce3dc578ef777d8343b474386d675401be18508a50fc->enter($__internal_a655d215d0b57cdfd8a0ce3dc578ef777d8343b474386d675401be18508a50fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1a5ed6016c0b7bd8d1054e695b36a2503992a6ff81f0897a89a5436aaa561825->leave($__internal_1a5ed6016c0b7bd8d1054e695b36a2503992a6ff81f0897a89a5436aaa561825_prof);

        
        $__internal_a655d215d0b57cdfd8a0ce3dc578ef777d8343b474386d675401be18508a50fc->leave($__internal_a655d215d0b57cdfd8a0ce3dc578ef777d8343b474386d675401be18508a50fc_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_7ef50e6b9d4359bfb33b215316d148ca8d5f0dd624094074fa4cc38df85793c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ef50e6b9d4359bfb33b215316d148ca8d5f0dd624094074fa4cc38df85793c4->enter($__internal_7ef50e6b9d4359bfb33b215316d148ca8d5f0dd624094074fa4cc38df85793c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_aff97027edb53d2648239fd8381184dc2e7421b5fe0321e0794860f781dad88a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aff97027edb53d2648239fd8381184dc2e7421b5fe0321e0794860f781dad88a->enter($__internal_aff97027edb53d2648239fd8381184dc2e7421b5fe0321e0794860f781dad88a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_aff97027edb53d2648239fd8381184dc2e7421b5fe0321e0794860f781dad88a->leave($__internal_aff97027edb53d2648239fd8381184dc2e7421b5fe0321e0794860f781dad88a_prof);

        
        $__internal_7ef50e6b9d4359bfb33b215316d148ca8d5f0dd624094074fa4cc38df85793c4->leave($__internal_7ef50e6b9d4359bfb33b215316d148ca8d5f0dd624094074fa4cc38df85793c4_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_86d638a315a504bde3873401946afbe0343a4baa2bdb6a48a3f99820d3415f8b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_86d638a315a504bde3873401946afbe0343a4baa2bdb6a48a3f99820d3415f8b->enter($__internal_86d638a315a504bde3873401946afbe0343a4baa2bdb6a48a3f99820d3415f8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_6e4267bea76a67f2bcac1d8ab59f25df33f79be38d0fd0e6fdf8181c0009bbde = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e4267bea76a67f2bcac1d8ab59f25df33f79be38d0fd0e6fdf8181c0009bbde->enter($__internal_6e4267bea76a67f2bcac1d8ab59f25df33f79be38d0fd0e6fdf8181c0009bbde_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_6e4267bea76a67f2bcac1d8ab59f25df33f79be38d0fd0e6fdf8181c0009bbde->leave($__internal_6e4267bea76a67f2bcac1d8ab59f25df33f79be38d0fd0e6fdf8181c0009bbde_prof);

        
        $__internal_86d638a315a504bde3873401946afbe0343a4baa2bdb6a48a3f99820d3415f8b->leave($__internal_86d638a315a504bde3873401946afbe0343a4baa2bdb6a48a3f99820d3415f8b_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_491c485fddf0528f61d9d29db509f85aa976db918d2d8aa6a96b62e88ff18ab9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_491c485fddf0528f61d9d29db509f85aa976db918d2d8aa6a96b62e88ff18ab9->enter($__internal_491c485fddf0528f61d9d29db509f85aa976db918d2d8aa6a96b62e88ff18ab9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_3ece9a412899ea12b0ad312a57d421df62c9bf78426c4cc45701e7c7a86b5256 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ece9a412899ea12b0ad312a57d421df62c9bf78426c4cc45701e7c7a86b5256->enter($__internal_3ece9a412899ea12b0ad312a57d421df62c9bf78426c4cc45701e7c7a86b5256_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_3ece9a412899ea12b0ad312a57d421df62c9bf78426c4cc45701e7c7a86b5256->leave($__internal_3ece9a412899ea12b0ad312a57d421df62c9bf78426c4cc45701e7c7a86b5256_prof);

        
        $__internal_491c485fddf0528f61d9d29db509f85aa976db918d2d8aa6a96b62e88ff18ab9->leave($__internal_491c485fddf0528f61d9d29db509f85aa976db918d2d8aa6a96b62e88ff18ab9_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
