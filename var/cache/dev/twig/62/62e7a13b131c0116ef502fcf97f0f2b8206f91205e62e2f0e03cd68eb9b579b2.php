<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_d2cb68099b3fe05741c41fd40c661f69a3aa1256a91caeea76fe0700544bfc54 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_905bb7f7d3fa7be98cd002bf783a93a6c5cdb7a0537834309073d5ecd77f82d7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_905bb7f7d3fa7be98cd002bf783a93a6c5cdb7a0537834309073d5ecd77f82d7->enter($__internal_905bb7f7d3fa7be98cd002bf783a93a6c5cdb7a0537834309073d5ecd77f82d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $__internal_90e4bb28e6d3b8dbcc66ca9500e6aafd0070496989f9502eee62e0781ba6f2bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_90e4bb28e6d3b8dbcc66ca9500e6aafd0070496989f9502eee62e0781ba6f2bc->enter($__internal_90e4bb28e6d3b8dbcc66ca9500e6aafd0070496989f9502eee62e0781ba6f2bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_905bb7f7d3fa7be98cd002bf783a93a6c5cdb7a0537834309073d5ecd77f82d7->leave($__internal_905bb7f7d3fa7be98cd002bf783a93a6c5cdb7a0537834309073d5ecd77f82d7_prof);

        
        $__internal_90e4bb28e6d3b8dbcc66ca9500e6aafd0070496989f9502eee62e0781ba6f2bc->leave($__internal_90e4bb28e6d3b8dbcc66ca9500e6aafd0070496989f9502eee62e0781ba6f2bc_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_dcd7ca5639ff32a18d35eb982c67f3398ca15c20117f76327203bc83976fa860 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dcd7ca5639ff32a18d35eb982c67f3398ca15c20117f76327203bc83976fa860->enter($__internal_dcd7ca5639ff32a18d35eb982c67f3398ca15c20117f76327203bc83976fa860_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_be07911ebd221395552d143f9cea1c72e38a389a5d8b05e3a7e18629feda17a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_be07911ebd221395552d143f9cea1c72e38a389a5d8b05e3a7e18629feda17a4->enter($__internal_be07911ebd221395552d143f9cea1c72e38a389a5d8b05e3a7e18629feda17a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_be07911ebd221395552d143f9cea1c72e38a389a5d8b05e3a7e18629feda17a4->leave($__internal_be07911ebd221395552d143f9cea1c72e38a389a5d8b05e3a7e18629feda17a4_prof);

        
        $__internal_dcd7ca5639ff32a18d35eb982c67f3398ca15c20117f76327203bc83976fa860->leave($__internal_dcd7ca5639ff32a18d35eb982c67f3398ca15c20117f76327203bc83976fa860_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/list_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:list.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/friendsofsymfony/user-bundle/Resources/views/Group/list.html.twig");
    }
}
