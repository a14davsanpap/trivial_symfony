<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_d401b2ea7f9f5826572a5fdc03202bd23da31dfaacdc84a4d1e8c5e32af05440 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2a12f0439fc806ef8765b2f2f315ea02369913da1baf865b0500af635161fd5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a12f0439fc806ef8765b2f2f315ea02369913da1baf865b0500af635161fd5a->enter($__internal_2a12f0439fc806ef8765b2f2f315ea02369913da1baf865b0500af635161fd5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $__internal_0f6b354e3f4309c8c72c912ab17a19c9ba6c4213d58ec7860e3be63acecfd313 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0f6b354e3f4309c8c72c912ab17a19c9ba6c4213d58ec7860e3be63acecfd313->enter($__internal_0f6b354e3f4309c8c72c912ab17a19c9ba6c4213d58ec7860e3be63acecfd313_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2a12f0439fc806ef8765b2f2f315ea02369913da1baf865b0500af635161fd5a->leave($__internal_2a12f0439fc806ef8765b2f2f315ea02369913da1baf865b0500af635161fd5a_prof);

        
        $__internal_0f6b354e3f4309c8c72c912ab17a19c9ba6c4213d58ec7860e3be63acecfd313->leave($__internal_0f6b354e3f4309c8c72c912ab17a19c9ba6c4213d58ec7860e3be63acecfd313_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_58e152af62977c4726092257611ba38044d2b5724626115ecb49937a7649d616 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_58e152af62977c4726092257611ba38044d2b5724626115ecb49937a7649d616->enter($__internal_58e152af62977c4726092257611ba38044d2b5724626115ecb49937a7649d616_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_28e3c3ce19e331b32d4e5f57353f1e98dc471cdc8cec0780cc6218ee81fc0c7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28e3c3ce19e331b32d4e5f57353f1e98dc471cdc8cec0780cc6218ee81fc0c7c->enter($__internal_28e3c3ce19e331b32d4e5f57353f1e98dc471cdc8cec0780cc6218ee81fc0c7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_28e3c3ce19e331b32d4e5f57353f1e98dc471cdc8cec0780cc6218ee81fc0c7c->leave($__internal_28e3c3ce19e331b32d4e5f57353f1e98dc471cdc8cec0780cc6218ee81fc0c7c_prof);

        
        $__internal_58e152af62977c4726092257611ba38044d2b5724626115ecb49937a7649d616->leave($__internal_58e152af62977c4726092257611ba38044d2b5724626115ecb49937a7649d616_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/reset_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:reset.html.twig", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/reset.html.twig");
    }
}
