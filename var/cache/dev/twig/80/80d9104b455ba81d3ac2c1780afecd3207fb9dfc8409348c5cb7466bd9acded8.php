<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_14006793ff0e0fc11971a0eb9ccd995c1b59242fb07f6c2c641d8b3cbcf95cac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_54a31a79a099f2729cb9baecd4f806f626eacd4757859842d8ef8d404d7109ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_54a31a79a099f2729cb9baecd4f806f626eacd4757859842d8ef8d404d7109ca->enter($__internal_54a31a79a099f2729cb9baecd4f806f626eacd4757859842d8ef8d404d7109ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_b1acb9a457d8d34ae20e19da0cf32feda983e85e09cf25dfbb0d92d8cfa56b3d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b1acb9a457d8d34ae20e19da0cf32feda983e85e09cf25dfbb0d92d8cfa56b3d->enter($__internal_b1acb9a457d8d34ae20e19da0cf32feda983e85e09cf25dfbb0d92d8cfa56b3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_54a31a79a099f2729cb9baecd4f806f626eacd4757859842d8ef8d404d7109ca->leave($__internal_54a31a79a099f2729cb9baecd4f806f626eacd4757859842d8ef8d404d7109ca_prof);

        
        $__internal_b1acb9a457d8d34ae20e19da0cf32feda983e85e09cf25dfbb0d92d8cfa56b3d->leave($__internal_b1acb9a457d8d34ae20e19da0cf32feda983e85e09cf25dfbb0d92d8cfa56b3d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/container_attributes.html.php");
    }
}
