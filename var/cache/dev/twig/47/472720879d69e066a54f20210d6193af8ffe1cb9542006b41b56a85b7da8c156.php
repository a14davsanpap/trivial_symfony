<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_e604c17d60b9bbb8f9cac0322a678f018970625992eb1bc06a579c5eff88914b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6084232c60f5392220aa041a5cafb2f6ca010d3b88e8ca0020fec4d5ea8362ce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6084232c60f5392220aa041a5cafb2f6ca010d3b88e8ca0020fec4d5ea8362ce->enter($__internal_6084232c60f5392220aa041a5cafb2f6ca010d3b88e8ca0020fec4d5ea8362ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        $__internal_2a1467325297ba1e0fa9583a13a5a6f51dbb4d7aa82ae8146284ebb73fdfe4de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a1467325297ba1e0fa9583a13a5a6f51dbb4d7aa82ae8146284ebb73fdfe4de->enter($__internal_2a1467325297ba1e0fa9583a13a5a6f51dbb4d7aa82ae8146284ebb73fdfe4de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_6084232c60f5392220aa041a5cafb2f6ca010d3b88e8ca0020fec4d5ea8362ce->leave($__internal_6084232c60f5392220aa041a5cafb2f6ca010d3b88e8ca0020fec4d5ea8362ce_prof);

        
        $__internal_2a1467325297ba1e0fa9583a13a5a6f51dbb4d7aa82ae8146284ebb73fdfe4de->leave($__internal_2a1467325297ba1e0fa9583a13a5a6f51dbb4d7aa82ae8146284ebb73fdfe4de_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/radio_widget.html.php");
    }
}
