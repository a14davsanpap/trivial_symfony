<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_fe527007acf5d27a058d7cc714e187ad085376dff826d0e7e109bf7efeaf0710 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dcb2e8406edffecaa4848f1eba45f022e268f8d5f316be48686c3c8c83ed6100 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dcb2e8406edffecaa4848f1eba45f022e268f8d5f316be48686c3c8c83ed6100->enter($__internal_dcb2e8406edffecaa4848f1eba45f022e268f8d5f316be48686c3c8c83ed6100_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_20f4741f8409c622a59399a704561e76c4c69afa0ecaadf9304ab47d2682f9c4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_20f4741f8409c622a59399a704561e76c4c69afa0ecaadf9304ab47d2682f9c4->enter($__internal_20f4741f8409c622a59399a704561e76c4c69afa0ecaadf9304ab47d2682f9c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_dcb2e8406edffecaa4848f1eba45f022e268f8d5f316be48686c3c8c83ed6100->leave($__internal_dcb2e8406edffecaa4848f1eba45f022e268f8d5f316be48686c3c8c83ed6100_prof);

        
        $__internal_20f4741f8409c622a59399a704561e76c4c69afa0ecaadf9304ab47d2682f9c4->leave($__internal_20f4741f8409c622a59399a704561e76c4c69afa0ecaadf9304ab47d2682f9c4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_end.html.php");
    }
}
