<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_05e4ba2fd294fdec1729ead3147bccf54b5ac9a204e9543a8a9ab09d10e5ee8d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7426299a2fbbbf3426660acb1ef1ac725d6bceb9f5cb21bd39f112ecc202588d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7426299a2fbbbf3426660acb1ef1ac725d6bceb9f5cb21bd39f112ecc202588d->enter($__internal_7426299a2fbbbf3426660acb1ef1ac725d6bceb9f5cb21bd39f112ecc202588d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_1c421812f8cf3d566c854537346b948b5b102e4bb6f262ba1aae20566b7112ba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c421812f8cf3d566c854537346b948b5b102e4bb6f262ba1aae20566b7112ba->enter($__internal_1c421812f8cf3d566c854537346b948b5b102e4bb6f262ba1aae20566b7112ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_7426299a2fbbbf3426660acb1ef1ac725d6bceb9f5cb21bd39f112ecc202588d->leave($__internal_7426299a2fbbbf3426660acb1ef1ac725d6bceb9f5cb21bd39f112ecc202588d_prof);

        
        $__internal_1c421812f8cf3d566c854537346b948b5b102e4bb6f262ba1aae20566b7112ba->leave($__internal_1c421812f8cf3d566c854537346b948b5b102e4bb6f262ba1aae20566b7112ba_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "/home/ausias/Escriptori/Projectas/trivialjoc_22032017/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget.html.php");
    }
}
